//put this in the notifications factory file when its done

const nowInUTC = new Date();
const nowInMilliSeconds = Date.parse(nowInUTC);
const reminderDateInMilliseconds = Date.parse("Sun Nov 29 2020 23:05:10 GMT+0000") //date value will be variable, get From QM Meeting Info Object, convert to utc

reminderTimeInMilliseconds = reminderDateInMilliseconds - nowInMilliSeconds;

const timeoutObj = setTimeout(() => {

//create a notification
//get meeting and reminder-data
//generate the notification card
//build a trigger and attach the reminder notification
//send a meeting_notification_trigger message activity request to the bot
//bot routes the activity to the correct teams-conversation-id (DESTINATION)

    console.log('reminder_notification_sent');
  }, reminderTimeInMilliseconds);


