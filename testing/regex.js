
const re = /(?<=<\/at>).*/;

const utterance = "<at>QM Assistant Staging</at> alerts";

const query = extractUserQuery(utterance);
console.log(query);

function extractUserQuery(utterance){

    const parse = re.exec(utterance);
    const query = parse[0].trim();
 
    return query;

}
