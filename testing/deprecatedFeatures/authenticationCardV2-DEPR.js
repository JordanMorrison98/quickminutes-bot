const {CardFactory} = require('botbuilder');

async function generateCard(){

    const card = CardFactory.adaptiveCard({
        "type": "AdaptiveCard",
        "body": [
            {
                "type": "TextBlock",
                "size": "Large",
                "weight": "Bolder",
                "text": "Action Required"
            },
            {
                "type": "TextBlock",
                "text": "Activate the QuickMinutes bot in seconds by simply signing in to your account. Don't have an account? You can register for a free account now!",
                "wrap": true
            },
            {
                "type": "ActionSet",
                "actions": [
                    {
                        "type": "Action.Submit",
                        "id": "btnAuth",
                        "title": "Login/Register",
                        "data": {
                            "taskModule": "authCard",
                            "msteams": {
                            "type": "task/fetch"
                            }
                          }
                        }
                ],
                "spacing": "Medium",
                "horizontalAlignment": "Center"
            }
        ],
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.2"
    });


        return card;

}

module.exports = {
    generateCard
}