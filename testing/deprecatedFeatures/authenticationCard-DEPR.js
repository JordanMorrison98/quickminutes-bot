const {CardFactory} = require('botbuilder');

async function generateCard(upn){

        const card = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "Large",
                    "weight": "Bolder",
                    "text": "Action Required"
                },
                {
                    "type": "TextBlock",
                    "text": "Activate the QuickMinutes bot in seconds by simply signing in to your account. Don't have an account? You can register for a free account now!",
                    "wrap": true
                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.OpenUrl", 
                            "title": "Login/Register", 
                            "url" : "https://staging.quickminutes.com/microsoft_token_auth?redirectUri=" + process.env.baseUrl + "/callback?uid=" + upn + "&uid=" + upn,                  
                        }
                    ],
                    "spacing": "Medium",
                    "horizontalAlignment": "Center"
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });

        return card;

}

module.exports = {
    generateCard
}