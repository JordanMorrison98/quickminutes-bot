const permissionsActivityHandler = require('../activityHandlers/featurePermissionsActivityHandler.js');


async function featureIsEnabledForUser(feature, aad_id, groupId){


    //pull Qm User Profile + Group Members from api here
    //get is_group_admin value for user
    
    var qmUserProfile = await permissionsActivityHandler.retrieveUserProfile(aad_id);

    var userId = qmUserProfile.id;

    var groupMembers = await permissionsActivityHandler.retrieveGroupListMembers(aad_id, groupId);

    //check if the user is a member of the group, if yes, get their user profile obj

    var user = await isMember(groupMembers, userId);

    switch (feature) {

        case 'notification_config':
            if(user.pivot.user_type == 'group_admin'){
                return true;
            }else{
                console.log("User with aad_id of " + aad_id + "is not enabled for this feature : " + feature)
                return false;
            }

        default:

        console.log("User is requesting an unknown feature.")
        return;
    }

  }

  async function isMember(groupMembers, userId) {

    for (var i = 0; i <= groupMembers.data.length; i++) {
        if (groupMembers.data[i].id == userId) {
            console.log("User is a member of the group.");
            return groupMembers.data[i];
        } else {
            console.log("User is NOT a member of the group.");
            return null;
        }


    }

}

  module.exports = {
 
    featureIsEnabledForUser,

  }


