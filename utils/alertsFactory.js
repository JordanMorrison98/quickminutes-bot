const {
    CardFactory
} = require('botbuilder');


class AlertsFactory {

  static async generateCard(alertType, alertMessage){

    const icon = await this.getAlertIcon(alertType);

    const alert = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "ColumnSet",
                    "columns": [
                        {
                            "type": "Column",
                            "width": "auto",
                            "items": [
                                {
                                    "type": "TextBlock",
                                    "text": alertType,
                                    "wrap": true,
                                    "weight": "Bolder",
                                    "size": "Large"
                                }
                            ],
                            "verticalContentAlignment": "Center"
                        },
                        {
                            "type": "Column",
                            "width": "stretch",
                            "items": [
                                {
                                    "type": "Image",
                                    "url": icon,
                                    "size": "Small",
                                    "height": "-8px",
                                    "width": "20px"
                                }
                            ],
                            "horizontalAlignment": "Center",
                            "verticalContentAlignment": "Center"
                        }
                    ]
                },
                {
                    "type": "ColumnSet",
                    "columns": [
                        {
                            "type": "Column",
                            "width": "auto",
                            "items": [
                                {
                                    "type": "TextBlock",
                                    "text": alertMessage,
                                    "wrap": true
                                }
                            ],
                            "height": "stretch",
                            "verticalContentAlignment": "Center"
                        }
                    ],
                    "separator": true
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });

    return alert;

  }

  static async getAlertIcon(alertType){

    switch (alertType) {
        case 'Info':

            return 'https://i.ibb.co/3ztFhmH/Info-1.png';
            
        case 'Warning':
        
            return 'https://i.ibb.co/GcQwMFd/Warning-2.png';

        case 'Success':

            return 'https://i.ibb.co/RH6Rsvp/Succesful-2.png';
    

        case 'Error':
         
            return 'https://i.ibb.co/r7JtBNk/Error-1.png';
    
        default:
            break;
    }

  }
  
}

module.exports.AlertsFactory = AlertsFactory;