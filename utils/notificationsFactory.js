// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

var axios = require('axios').default;

const {
    CardFactory
} = require('botbuilder');


const notificationHandler = require('../activityHandlers/notificationActivityHandler.js');

class NotificationsFactory {

    static async send(notificationRecord, meetingId, notificationType, aad_id){

        /**
         * Martin should save the aad_id (Qm Assistant id) to the users QM account. 
         * This will pair the QM assistant to their QM Platform account
         * Send the aad_id of the user in every notification payload so I can retrieve valid data on the QM API
         * Benefits of this approach:
         * 1) Once a pairing has been made, this will make it easier for communication 
         * between the bot and the platform in future features
         * 2) We don't have to depend on the data QM sends in notification payloads. I define what and how the 
         * notifications data is displayed on my end using the QM API as the single source of truth. 
         * The notification payload just gives me Primary and Foreign Keys for the API
         */

        var notificationCard = await NotificationsFactory.generateCard(notificationRecord, notificationType, meetingId, aad_id);

        var trigger = await NotificationsFactory.buildTrigger(notificationCard, notificationRecord.destinationInfo);

        try{
            const resp = await axios({
                method: 'POST',
                url: process.env.baseUrl + '/api/messages',
                headers: { 
                    'Authorization': process.env.botToken,
                    'Content-Type': 'application/json'
                },
                data : trigger
        });

      
        return resp.data;

        } catch (err) {
            //log an error if the notification doesn't send ==> send email to lead dev
            console.error(err);
        }

    }

    static async generateCard(notificationRecord, notificationType, meetingId, aad_id) {

       var meetingPayload = await notificationHandler.retrieveMeetingById(meetingId, aad_id, notificationType).then(async function(data) {

        var _dateTime = data.start_date.split(" ");


        
        //date format = dd/mm/yyyy
        //time format = hh:mm
        //duration format = hh hour(s) mm min(s)

        var date = NotificationsFactory.formatDate(_dateTime);
        var time = NotificationsFactory.formatTime(_dateTime);
        var duration = NotificationsFactory.formatDuration(data);

        //validation of meeting payload data

           var _meetingPayload = {
            "notification_type": notificationType,
            "group_id": notificationRecord.groupId,
            "meeting_title" : data.title,
            "user_id": data.creator_id,
            "location": data.location !== '' ? data.location : "Unknown",
            "start_date": date,
            "start_time" : time,
            "duration": duration,
            "created_at": data.created_at,
            "created_by": notificationRecord.groupName, // TO-DO: change to admin name
            "conference_link": data.conference,
            "published": data.published === 0 ? "Un-Published" : "Published"
           }

           // console.log(meetingPayload);

           return  _meetingPayload;

        });

        
         let meeting_pack_link_bool = false;
         let conference_link_bool = false;
         let location_field_bool = false;

        //set visible flag to false so you don't render call or meeting pack links for create/delete meeting notifications
         if(meetingPayload.notification_type == 'Meeting Scheduled' || meetingPayload.notification_type === 'Meeting Deleted'){
              meeting_pack_link_bool = false;
              conference_link_bool = false;
              location_field_bool = false;
         }else if(meetingPayload.notification_type == 'Meeting Details Updated'){
            location_field_bool = false;
         }else{
             meeting_pack_link_bool =  true;
            conference_link_bool = true;
            location_field_bool = true;
         }
        
         
         if(process.env.baseUrl == "https://qmassistant-prod.azurewebsites.net"){
            const prodUrl = "https://quickminutes.com/app#";
            var url = prodUrl
        
          }else{
            const stagingUrl = "https://staging.quickminutes.com/app#";
            var url = stagingUrl;
          }

          try {

            var notificationCard = CardFactory.adaptiveCard({
                "type": "AdaptiveCard",
                "body": [
                    {
                        "type": "TextBlock",
                        "size": "Large",
                        "weight": "Bolder",
                        "text": meetingPayload.meeting_title,
                        "wrap": true
                    },
                    {
                        "type": "TextBlock",
                        "text": meetingPayload.notification_type,
                        "wrap": true,
                        "size": "Medium",
                        "isSubtle": true,
                        "height": "stretch",
                        "spacing": "Small"
                    },
                    {
                        "type": "FactSet",
                        "facts": [
                            {
                                "title": "Location:",
                                "value": meetingPayload.location
                            }
                        ],
                        "separator": true,
                        "spacing": "Large",
                        "isVisible" : location_field_bool
                    },
                    {
                        "type": "FactSet",
                        "facts": [
                            {
                                "title": "Date: ",
                                "value": meetingPayload.start_date
                            }
                        ]
                    },
                    {
                        "type": "FactSet",
                        "facts": [
                            {
                                "title": "Start Time:",
                                "value": meetingPayload.start_time
                            }
                        ]
                    },
                    {
                        "type": "FactSet",
                        "facts": [
                            {
                                "title": "Duration:",
                                "value": meetingPayload.duration
                            }
                        ]
                    },
                    {
                        "type": "ActionSet",
                        "actions": [
                            {
                                "type": "Action.OpenUrl",
                                "title": "📞  MS Teams Call",
                                "url": meetingPayload.conference_link,
                                "style": "positive"
                            }
                        ],
                        "spacing": "Large",
                        "horizontalAlignment": "Center",
                        "isVisible" : conference_link_bool
                    },
                    {
                        "type": "ActionSet",
                        "actions": [
                            {
                                "type": "Action.OpenUrl",
                                "title": "📁  Meeting Pack",
                                "url": (url + "/group/" + meetingPayload.group_id + "/meeting/" + meetingId),
                                "style": "positive"
                            }
                        ],
                        "horizontalAlignment": "Center",
                        "spacing": "Large",
                        "isVisible" : meeting_pack_link_bool
                    }
                ],
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "version": "1.2"
            });

            console.log(notificationCard);
    
            return notificationCard;
              
          } catch (error) {
            //log an error if the notification card doesn't build + render
            console.error(err);
          }
 

        
       
    }


    static async buildTrigger(notificationCard, destinationInfo) {

        //first figure out where and who to send the notifcation to
        //then input these variables in the trigger + notification
        //send notification

        try {

            var trigger = {
                "text": "meeting_notification_trigger",
                "textFormat": "plain",
                "type": "message",
                "data": notificationCard,
                "channelId": "msteams",
                "serviceUrl": destinationInfo.serviceUrl,
                "from": destinationInfo.from,
                "conversation": destinationInfo.conversation,
                "recipient": destinationInfo.recipient,
                "entities": [{
                    "locale": "en-US",
                    "country": "US",
                    "platform": "Web",
                    "type": "clientInfo"
                }],
                "channelData": destinationInfo.channelData,
                "locale": "en-US"
            };

           // console.log(trigger);
    
            return trigger;
            
        } catch (error) {
            //log an error if the notification trigger doesn't build
            console.error(err);
        }

     

    };

    //format helper functions

    static formatTime(_dateTime) {
        var _time = _dateTime[1].split(":");
        var time = (_time[0] + ":" + _time[1]);
        return time;
    }

    static formatDate(_dateTime) {
        var _date = _dateTime[0].split("-");
        var date = (_date[2] + "/" + _date[1] + "/" + _date[0]);
        return date;
    }

    static formatDuration(data){
        var _duration = data.duration.split(":");
        var duration = " ";

        //format times with hh:mm
        //hours and minutes
        //hours and minute
        //hour and minutes
        //hour and minute

        if(_duration[0] !== "00" && _duration[1] !== "00"){
            duration = (_duration[0] + " hours " +  _duration[1] + " minutes");
        }else if(_duration[0].charAt(1) > "1" && _duration[1].charAt(1) === "1"){
            duration = (_duration[0] + " hours " +  _duration[1].charAt(1) + " minute");
        }else if(_duration[0].charAt(1) === "1" && _duration[1].charAt(1) > "1"){
            duration = (_duration[0] + " hour " +  _duration[1].charAt(1) + " minutes");
        }else if(_duration[0].charAt(1) === "1" && _duration[1].charAt(1) === "1"){
            duration = (_duration[0].charAt(1) + " hour " +  _duration[1].charAt(1) + " minute");
        }
            

        if(_duration[0] == "00"){
            //duration only has minutes
            //minutes or minute
            if(_duration[1] !== '00' || _duration[1] !== '01'){
                if(_duration[0].charAt(0) == "0"){
                     // minutes > 2
                    duration = (_duration[1] + " minutes");
                }else{
                    duration = (_duration[1] + " minutes");
                }    
            }else{
                // minutes <= 2
                duration = (_duration[0].charAt(1) + " hour");
            }
        }
        
        if(_duration[1] == '00'){
            //duration only has hours
            //hours or hour
            if(_duration[0] == '00' || _duration[0] == '01'){
                  // minutes <= 2
                  if(_duration[0].charAt(0) == "0"){
                    // minutes > 2
                   duration = (_duration[0].charAt(1) + " hour");
               }else{
                   duration = (_duration[0] + " hour");
               }  
            }else{
                if(_duration[0].charAt(0) == "0"){
                    // minutes > 2
                   duration = (_duration[0].charAt(1) + " hours");
               }else{
                   duration = (_duration[0] + " hours");
               }    
            }
        } 
    
        return duration;
    }

    
 
}

module.exports.NotificationsFactory = NotificationsFactory;