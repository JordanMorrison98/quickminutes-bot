// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ConfirmPrompt, DialogSet, DialogTurnStatus, OAuthPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const axios = require('axios').default;
const { LogoutDialog } = require('./logoutDialog');

const CONFIRM_PROMPT = 'ConfirmPrompt';
const MAIN_DIALOG = 'MainDialog';
const MAIN_WATERFALL_DIALOG = 'MainWaterfallDialog';
const OAUTH_PROMPT = 'OAuthPrompt';

//import analytics service
const { MixPanel } = require('../services/analytics/mixPanel.js');


class MainDialog extends LogoutDialog {
    constructor() {
        super(MAIN_DIALOG, process.env.connectionName);

        this.addDialog(new OAuthPrompt(OAUTH_PROMPT, {
            connectionName: process.env.connectionName,
            text: 'Activate the app by signing into your QuickMinutes account.',
            title: 'Login / Signup',
            timeout: 300000
        }));
        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
        this.addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
            this.promptStep.bind(this),
            this.loginStep.bind(this),
            // this.displayTokenPhase1.bind(this),
            // this.displayTokenPhase2.bind(this),
        ]));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a DialogContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} dialogContext
     */
    async run(context, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);
        const dialogContext = await dialogSet.createContext(context);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty || context.activity.userIsAuthenticated.result === false) {
            await dialogContext.beginDialog(this.id);
        }
    }

    

    async promptStep(stepContext) {
        return await stepContext.beginDialog(OAUTH_PROMPT);
    }

    async loginStep(stepContext) {
        // Get the token from the previous step. Note that we could also have gotten the
        // token directlyhey from the prompt itself. There is an example of this in the next method.
        const tokenResponse = stepContext.result;
        if (tokenResponse) {

            /**
             * TO-DO: Add silent authentication with QM platform here using the successful 
             * authentication with the identity provider (Microsoft AD for v1)
             * Send a GET request using axios that gives QM the data points it needs to 
             * sign the user in successfully
             * Also send the users aad_id so QM can save it to their dB
             * Make sure QM still sends the qm_api_token to the /callback endpoint
             * Along with the user's aad_id
             * This is needed to create the users QM assistant account and sign them into the qmBot. 
             */

            const aad_id = stepContext.context.activity.from.aadObjectId;
            const result = await signUserIntoQuickMinutes(stepContext, aad_id);

            if(result){
                //set user authenticated to true
                stepContext.context.activity.userIsAuthenticated = {result : true, aad_id: aad_id};
                MixPanel.logEvent('Sign In', stepContext.context, 'Success');  
                await stepContext.context.sendActivity('Login was successful. Type **help** to learn how I can assist you.');
            }else{
                MixPanel.logEvent('Sign In', stepContext.context, 'Failed');
                await stepContext.context.sendActivity('Sorry, we couldn\'t log you in. Please type **logout** and then try logging in again.');
            }
        }else{
            MixPanel.logEvent('Sign In', stepContext.context, 'Failed');
            await stepContext.context.sendActivity('Login was not successful please try again.');
        }
        
        return await stepContext.endDialog();
    }

}

async function signUserIntoQuickMinutes(stepContext, aad_id){

    const token = stepContext.result.token;

    //this is where I set up the login axios GET request to QuickMinutes /microsoft_token_auth endpoint
    //we need to authenticate the user within the bot and then use a Generic Auth Provider (auth 1.2.4) token to sign them in on QM.
    if(process.env.baseUrl == "https://qmassistant-prod.azurewebsites.net"){
        var qmBaseUrl = process.env.QUICKMINUTES_PRODUCTION_URL;
    }else{
        var qmBaseUrl = process.env.QUICKMINUTES_STAGING_URL;
    }
        
    try{
        const resp = await axios({
            method: 'GET',
            url: qmBaseUrl + '/microsoft_token_auth?redirectUri=' + process.env.baseUrl + "/callback?uid=" + aad_id + "&uid=" + aad_id + "&token=" + token,
            headers:  { 
            'Accept': 'application/json, text/plain, */*', 
            'Content-Type': 'application/json;charset=UTF-8', 
            'Connection' : 'keep-alive'
            },
        });

    
        //console.log(resp.data);
    
        return true;
    
    } catch (err) {
    // Handle Error Here
    console.error(err);
    return false;
    
    }
      

}
   

module.exports.MainDialog = MainDialog;

