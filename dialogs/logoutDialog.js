// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ActivityTypes } = require('botbuilder');
const { ComponentDialog } = require('botbuilder-dialogs');

//import analytics service
const { MixPanel } = require('../services/analytics/mixPanel.js');

// Import dB 
const firestoreDb = require('../services/firestoreDb.js');

class LogoutDialog extends ComponentDialog {
    constructor(id, connectionName) {
        super(id);
        this.connectionName = connectionName;
    }

    async onBeginDialog(innerDc, options) {
        const result = await this.interrupt(innerDc);
        if (result) {
            return result;
        }

        return await super.onBeginDialog(innerDc, options);
    }

    async onContinueDialog(innerDc) {
        const result = await this.interrupt(innerDc);
        if (result) {
            return result;
        }

        return await super.onContinueDialog(innerDc);
    }

    async interrupt(innerDc) {
        if (innerDc.context.activity.type === ActivityTypes.Message) {
            const userIntent = innerDc.context.activity.userIntent;
            if (userIntent === 'SignOut') {

                // The bot adapter encapsulates the authentication processes.
                const botAdapter = innerDc.context.adapter;
                await botAdapter.signOutUser(innerDc.context, this.connectionName);

                //Sign user out of their QuickMinutes Account (delete qm_api_token)
                firestoreDb.signUserOut(innerDc.context.activity.aad_id);
                MixPanel.logEvent('Sign Out', innerDc.context, 'Success');  
                await innerDc.context.sendActivity('You have been signed out. Type **login** at any time to sign in again.');
                return await innerDc.cancelAllDialogs();

                
            }
        }
    }

    async interruptV2(innerDc) {
        if (innerDc.context.activity.type === ActivityTypes.Message) {
            const text = innerDc.context.activity.userIntent;
            // The bot adapter encapsulates the authentication processes.
            const botAdapter = innerDc.context.adapter;
            if (text === 'SignOut') {
                firestoreDb.signUserOut(innerDc.context.activity.aad_id);
                MixPanel.logEvent('Sign Out', innerDc.context, 'Success');  
            }
            await botAdapter.signOutUser(innerDc.context, this.connectionName); 
            await innerDc.context.sendActivity('You have been signed out. Type **login** at any time to sign in again.');
            return await innerDc.cancelAllDialogs();

        }
    }
}

module.exports.LogoutDialog = LogoutDialog;
