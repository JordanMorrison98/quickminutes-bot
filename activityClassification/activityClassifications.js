class ActivityClassifications { 

    //these activities map to intents supported in this version of the QM Assistant

    constructor(){

        this.UserIsRequestingNotificationConfigurationCard = "NotificationConfiguration";
        this.UserWantsToSubscribeToMeetingNotifications = "NotificationsSubscription";
        this.UserIsSayingHello = "Hello";
        this.UserIsRequestingTakeATourCard = "TakeATour";
        this.UserWantsToSignOut = "SignOut";
        this.UserWantsToSignIn = "SignIn";
        this.UserWantsToViewMeetingList = "MeetingList";
        this.UserWantsHelp = "Help";
        this.UserWantsToViewMeetingSummary = "MeetingSummary";
        this.UserWantsToRemoveNotifications = "RemoveNotifications";
        this.UserWantsRemoveOneNotification = "RemoveOneNotification";

    }
    




    
}

module.exports.ActivityClassifications = ActivityClassifications;