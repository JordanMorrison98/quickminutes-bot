// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

// index.js is used to setup and configure your bot

// Import required pckages
const path = require('path');
const express = require('express');
const bodyParser = require("body-parser");
var axios = require('axios').default;

// Import required bot services.
// See https://aka.ms/bot-services to learn more about the different parts of a bot.
const { BotFrameworkAdapter,ConversationState, MemoryStorage, UserState, TeamsInfo} = require('botbuilder');

// Import bos 
const { QmBot } = require('./qmBot');
const { TeamsBot } = require('./bots/teamsBot');
const { MainDialog } = require('./dialogs/mainDialog');

//Import LUIS Prediction Service
const LUIS = require('./services/luis.js');

//Import Services
const firestoreDb = require('./services/firestoreDb.js');
const { Notifications } = require('./services/notifications.js');

// Read botFilePath and botFileSecret from .env file.
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about adapters.
const adapter = new BotFrameworkAdapter({
    appId: process.env.BotId,
    appPassword: process.env.BotPassword
});

var insights_key = getTarget();

const appInsights = require('applicationinsights');
appInsights.setup(insights_key)
    .setAutoDependencyCorrelation(true)
    .setAutoCollectRequests(true)
    .setAutoCollectPerformance(true, true)
    .setAutoCollectExceptions(true)
    .setAutoCollectDependencies(true)
    .setAutoCollectConsole(true, true)
    .setUseDiskRetryCaching(true)
    .setSendLiveMetrics(true)
    .setDistributedTracingMode(appInsights.DistributedTracingModes.AI)
    .start();

adapter.onTurnError = async (context, error) => {
    // This check writes out errors to console log .vs. app insights.
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    console.error(`\n [onTurnError] unhandled error: ${ error }`);

    // Send a trace activity, which will be displayed in Bot Framework Emulator
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${ error }`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );

    // Send a message to the user
    await context.sendActivity('The bot encountered an error or bug.');
    await context.sendActivity(`Technical dump: ${ error }`);

    // Clear out state
    await conversationState.delete(context);
};

// Define the state store for your bot.
// See https://aka.ms/about-bot-state to learn more about using MemoryStorage.
// A bot requires a state storage system to persist the dialog and user state between messages.
const memoryStorage = new MemoryStorage();

// Create conversation and user state with in-memory storage provider.
const conversationState = new ConversationState(memoryStorage);
const userState = new UserState(memoryStorage);

// Create the main dialog.
const dialog = new MainDialog();

// Create bot handler
const qmBot = new QmBot(userState);
// Create the bot that will handle incoming messages.
const qmAuthBot = new TeamsBot(conversationState, userState, dialog);


// Create HTTP server
const server = express();

const port = process.env.port || process.env.PORT || 3978;
server.listen(port, () => 
    console.log(`\Bot/ME service listening at http://localhost:${port}`)   
);


//setting up html rendering functionality
server.set('view engine', 'ejs');
server.engine('html', require('ejs').renderFile);

const publicDir = require('path').join(__dirname,'/public'); 
server.use(express.static(publicDir)); 

// support parsing of application/json type post data
server.use(bodyParser.json());


// notifications webhook
server.post("/hook", (req, res) => { 

    /**
     * Every notification sent by QuickMinutes comes through this route
     * Use defensive programming to validate notifications + log any errors to application insights service
     **/

    if (JSON.stringify(req.body) === '{}') {
      //if QM sends an empty payload to this endpoint log the error to app insights service and send email alert to lead dev. 
      console.error("Empty Payload Sent from Quickminutes \n ");
      console.error("Here is the notification payload sent from QuickMinutes ==> ");
      console.error(req.body);
      res.status(502).send("Empty Payload Sent from Quickminutes \n "); // Responding with 502 bad gateway error, because the upstream server (QM) didn't send any data
    }else if(
       req.body.group_id === ' ' ||
       req.body.meeting_id === ' ' ||
       req.body.notification_type === ' ' ||
       req.body.uuid === ' '
    ) {
      //if QM sends a payload to this endpoint, but one of the data points is undefined then log the error to app insights service and send email alert to lead dev. 
      console.error("Invalid Payload Sent from Quickminutes \n ");
      console.error("Here is the notification payload sent from QuickMinutes ==> ");
      console.error(req.body);
      res.status(502).send(req.body); // Responding with 502 bad gateway error, because the upstream server (QM) sent incorrect wrong data
    }else{
      var groupId = req.body.group_id.toString(); //convert query ids as string for firebase dB
      var meetingId = req.body.meeting_id;
      var notificationType = req.body.notification_type;
      var aad_id = req.body.uuid; //Azure Active Directory ID (aad_id) is the bot's Primary Key
      //create and send notification
      Notifications.getNotificationRecordsByGroupId(groupId, meetingId, notificationType, aad_id);
      console.info("Here is the notification payload sent from QuickMinutes ==> ");
      console.info(req.body);
      res.status(200).end(); // Responding with 200 OK response if successful
    }
})


server.get("/callback", (req, res) => { 

 /** 
  * Check if the user who has successfully authenticated with QM has a user profile on the dB
  * if yes, then set the bearer token to the user profile using their aad_id as a primary key
  * if not, then throw error message
  */
    
    //this is where QM redirects the access token after the auth flow
    if(req.headers.bearer){
      console.log("This is your access token: " + req.headers.bearer);
      const bearerToken = ("Bearer " + req.headers.bearer);
      const aad_id = getAadId(req);
      firestoreDb.createUserProfile(bearerToken, aad_id);
      //show sign in status screen if the auth window doesn't close automatically
      res.render('index.html');  
    }
    
  })

  // Listen for incoming requests.
  server.use('/api/messages', (req, res) => {

    //catch the bot auth token here for notification triggers
    process.env.botToken = req.headers.authorization;

    adapter.processActivity(req, res, async (context) => {

       //get the users aad_id (uuid for user)
       //check if this users profile exists or has a valid api access token on the dB
       //if it doesn't, then send them through auth dialogue to get a qm_api_token
       //if it does, update the profile with their teams user data
        if(context.activity.text){
            context.activity.userIntent = await LUIS.processUserRequest(context); //send user utterance to LUIS Prediction service for NLP Processing
        }
        const userIsAuthenticated = await authenticateUser(context);
        //attach the user authenticated object to the context object so it can be accessed in the qmBot or qmAuthBot
        context.activity.userIsAuthenticated = userIsAuthenticated;
        //if the action maps to an intent, attach intent to the context object for processing
        LUIS.mapActionToIntent(context);

        if(userIsAuthenticated.result){   
            context.activity.aad_id = userIsAuthenticated.aad_id; //appending aad_id for identifying user through multi-turn interactions
            if(context.activity.userIntent == 'SignOut'){
              await qmAuthBot.run(context); //sign the user out
            }else{
              await qmBot.run(context); //send the users' intent to qmBot for processing
            }   
        }else{
            try {
                //show take a tour card and feedback form even if the user isn't authenticated  
                //To-Do- Refactor this logic to make it cleaner
                if(context.activity.userIntent == "TakeATour"){
                    await qmBot.run(context);
                }else if(context.activity.userIntent == "Help"){
                    await qmBot.run(context);
                }else if(context.activity.userIntent == "SignOut"){
                    // Send a message to the user
                    await context.sendActivity('You are already signed out. Please type **login** to sign in again.');
                }else if(context.activity.name == "signin/verifyState"){
                    await qmAuthBot.run(context); //get the user to authenticate with QuickMinutes
                }else if(context.activity.type == "invoke"){
                    await qmBot.run(context);
                }else{
                    await qmAuthBot.run(context); //get the user to authenticate with QuickMinutes
                }
            } catch (error) {
                await qmAuthBot.run(context); //get the user to authenticate with QuickMinutes
            }
           }
        });
    });

function getTarget() {
    var insights_key = '';

    switch (process.env.baseUrl) {
        case 'https://qmassistant-prod.azurewebsites.net':
            insights_key = process.env.APPLICATION_INSIGHTS_KEY_PROD;
            break;
        case 'https://qmassistant-stage.azurewebsites.net':
            insights_key = process.env.APPLICATION_INSIGHTS_KEY_STAGE;
            break;

        default:
            insights_key = process.env.APPLICATION_INSIGHTS_KEY_LOCAL;
            break;
    }
    return insights_key;
}

 function getAadId(req) {
    var userIdString1 = req.url.split("?");
    var userIdString2 = userIdString1[1].split("=");
    var aad_id = userIdString2[1];
    return aad_id;
}

async function authenticateUser(context) {
    const user = await TeamsInfo.getMember(context, encodeURI(context.activity.from.aadObjectId));  //if the user has a profile, then update it with their teams data
    context.activity.userProfile = user;
    //then send the activity to the qmBot for processing
    //if not then send them to through the auth flow to get qm_api_token
    return firestoreDb.userHasToken(user.aadObjectId);
}



module.exports = {
    qmAuthBot
}



