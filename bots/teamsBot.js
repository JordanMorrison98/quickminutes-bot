// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { DialogBot } = require('./dialogBot');
const welcomecardGenerator = require('../cards/welcomeCard.js');
// Welcomed User property name
const WELCOMED_USER = 'welcomedUserProperty';


class TeamsBot extends DialogBot {
    /**
     *
     * @param {ConversationState} conversationState
     * @param {UserState} userState
     * @param {Dialog} dialog
     */
    constructor(conversationState, userState, dialog) {
        super(conversationState, userState, dialog);

        // Creates a new user property accessor.
        // See https://aka.ms/about-bot-state-accessors to learn more about the bot state and state accessors.
        this.welcomedUserProperty = userState.createProperty(WELCOMED_USER);
        
        this.userState = userState;

        //https://github.com/microsoft/BotBuilder-Samples/blob/main/samples/javascript_nodejs/03.welcome-users/bots/welcomeBot.js

            // Sends welcome messages to conversation members when they join the conversation.
            // Messages are only sent to conversation members who aren't the bot.
            this.onMembersAdded(async (context, next) => {

                const type = context.activity.conversation.conversationType;
                const gcWelcome = await this.welcomedUserProperty.get(context, false);
                const teamWelcome = await this.welcomedUserProperty.get(context, false);
                
                switch (type) {

                    case 'groupChat':

                        // Iterate over all new members added to the conversation
                        for (const idx in context.activity.membersAdded) {

                            if (context.activity.membersAdded[idx].id === context.activity.recipient.id && gcWelcome == false) {
                                // Set the flag indicating the bot has welcomed everyone in the groupchat
                                await this.welcomedUserProperty.set(context, true);
                                var welcomeCard = await welcomecardGenerator.generateCard();
                                await context.sendActivity({ attachments: [welcomeCard] });
                                
                            }
                        }

                        break

                    case 'personal':

                        var welcomeCard = await welcomecardGenerator.generateCard();
                        await context.sendActivity({ attachments: [welcomeCard] });
                    
                        break;

                    case 'channel':

                       // Iterate over all new members added to the conversation
                       for (const idx in context.activity.membersAdded) {

                        if (context.activity.membersAdded[idx].id === context.activity.recipient.id && teamWelcome == false) {
                            // Set the flag indicating the bot has welcomed everyone in the groupchat
                            await this.welcomedUserProperty.set(context, true);
                            var welcomeCard = await welcomecardGenerator.generateCard();
                            await context.sendActivity({ attachments: [welcomeCard] });
                            
                        }
                    }
                
                    default:
                        break;
                }


                // By calling next() you ensure that the next BotHandler is run.
                await next();
            });
    }

    async handleTeamsSigninVerifyState(context, state) {
        await this.dialog.run(context, this.dialogState);
    }
}

module.exports.TeamsBot = TeamsBot;
