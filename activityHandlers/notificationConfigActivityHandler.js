/**
 * 
 * This is where all of the data needed for the notificationsConfigCard is retrieved from the QM API and then manipulated
 * Once the data is ready, send it to the notificationsConfigCard for rendering
 * 
 */

// Import API
const api = require('../services/api.js');
// Import dB
const firestoreDb = require('../services/firestoreDb.js');


/** 
 * Pull all user groups from QM
 * Then massage this data into a clean format. 
 * Then send this data to the notifications card generator
*/


//get the users aad_id on every request. 
//pull the users qm_api_token from the firestore db
//inject this token into every api request they send


 async function retrieveGroups(aad_id){

    var userProfile = await firestoreDb.pullUserQmApiToken(aad_id);

    // console.log(userProfile.qm_api_token);

    var groups = api.qmAPI(userProfile.qm_api_token, 'POST', 'groups/list', null).then(async function(data) {

        // write some methods that validate the data and parse into correct format for the card
        var _groups = []

        data.forEach(group => {
            _groups.push({"id" : group.id.toString() , "name" : group.name}) 
        })

        return  _groups;

    })

    return await groups;

} 








module.exports = {
    retrieveGroups,
}

   


