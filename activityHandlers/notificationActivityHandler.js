/**
 * 
 * This is where all of the data needed for the notificationsCard is retrieved from the QM API and then manipulated
 * Once the data is ready, send it to the notificationsCard for rendering
 * 
 */

// Import API
const api = require('../services/api.js');
// Import dB
const firestoreDb = require('../services/firestoreDb.js');


/** 
 * Pull all user groups from QM
 * Then massage this data into a clean format. 
 * Then send this data to the notifications card generator
 * 
 * 
*/

 async function retrieveMeetingById(meetingId, aad_id, notificationType){

    const userProfile = await firestoreDb.pullUserQmApiToken(aad_id);

    const body = {"meeting_id": meetingId}

    //console.log(userProfile.qm_api_token);

    if(notificationType === 'Meeting Deleted'){

        var meeting = api.qmAPI(userProfile.qm_api_token, 'POST', 'meeting/get_deleted', body).then(async function(_meeting) {
     
            return  _meeting;
    
        })
    
        
        return await meeting;
        
    }else{

        var meeting = api.qmAPI(userProfile.qm_api_token, 'POST', 'meeting/get', body).then(async function(_meeting) {
     
            return  _meeting;
    
        })
    
        
        return await meeting;

    }


} 


module.exports = {
    retrieveMeetingById
}

