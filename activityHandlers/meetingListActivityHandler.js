// Import API
const api = require('../services/api.js');
// Import dB
const firestoreDb = require('../services/firestoreDb.js');

async function getUpcomingMeetings(aad_id){
   
    const groups = await retrieveGroups(aad_id);
    const _allUpcomingMeetings = await retrieveAllUpcomingMeetings(aad_id, groups);
    //flatten the nested array for cleaner data manipulation 
    //https://stackoverflow.com/questions/10865025/merge-flatten-an-array-of-arrays
    const allUpcomingMeetings = [].concat.apply([], _allUpcomingMeetings)
    return {meetings : allUpcomingMeetings, groups : groups};
 }

 async function retrieveAllUpcomingMeetings(aad_id, groups){

    var meetings = [];

     for (group of groups){
         const results = await retrieveMeetings(aad_id, group.id).then(async function(data){
            //this is where all of the meetings for the group id return
            //create a master meetings object with all upcoming meetings for user
            //console.log(data.length);
            return data;
          });  
         //console.log(results);
         meetings.push(results);
     }
     
     return meetings;
        
 }

 async function retrieveGroups(aad_id){

    const userProfile = await firestoreDb.pullUserQmApiToken(aad_id);

    const groups = api.qmAPI(userProfile.qm_api_token, 'POST', 'groups/list', null).then(async function(data) {

        // write some methods that validate the data and parse into correct format for the card
        const _groups = []

        data.forEach(group => {
            _groups.push({"id" : group.id , "name" : group.name}) 
        })

        return  _groups;

    })

    return await groups;

 }

 async function retrieveMeetings(aad_id, id){

    const userProfile = await firestoreDb.pullUserQmApiToken(aad_id);
    const body = {"group_id" : id};
    const meetings = api.qmAPI(userProfile.qm_api_token, 'POST', 'groups/list_meetings', body).then(async function(_meetings) {
        // write some methods that validate the data and parse into correct format for the card
        return _meetings.upcoming_meetings_unarchived;
    })

    return await meetings;

} 


module.exports = {
    getUpcomingMeetings
}

