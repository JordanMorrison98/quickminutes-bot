/**
 * 
 * This is where all of the data needed for the notificationsCard is retrieved from the QM API and then manipulated
 * Once the data is ready, send it to the notificationsCard for rendering
 * 
 */

// Import API
const api = require('../services/api.js');
// Import dB
const firestoreDb = require('../services/firestoreDb.js');


/** 
 * Pull all user groups from QM
 * Then massage this data into a clean format. 
 * Then send this data to the notifications card generator
 * 
 * 
*/

 async function retrieveGroupListMembers(aad_id, groupId){

    var userProfile = await firestoreDb.pullUserQmApiToken(aad_id);

    var body = {
        "group_id" : groupId
      }

    //console.log(userProfile.qm_api_token);

    var members = api.qmAPI(userProfile.qm_api_token, 'POST', 'groups/list_members', body).then(async function(_members) {

        // write some methods that validate the data and parse into correct format for the card
 
        return  _members;

    })

    
    return await members;

} 

async function retrieveUserProfile(aad_id){

    var userProfile = await firestoreDb.pullUserQmApiToken(aad_id);

    //console.log(userProfile.qm_api_token);

    var profile = api.qmAPI(userProfile.qm_api_token, 'GET', 'user/get', null).then(async function(_profile) {

        // write some methods that validate the data and parse into correct format for the card
        //use OOP class/model constructors here?
 
        return  _profile;

    })

    
    return await profile;

} 


module.exports = {
    retrieveGroupListMembers,
    retrieveUserProfile
}

