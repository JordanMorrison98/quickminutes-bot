// Import API
const api = require('../services/api.js');
// Import dB
const firestoreDb = require('../services/firestoreDb.js');
//import meeting model
const { Meeting } = require('../models/Meeting');

//use meeting id to get that meetings data on qm api
//then massage data into formatted data using the meeting model


async function getFormattedMeeting(aad_id, id){
    const meeting = await retrieveMeeting(aad_id, id);
    const m = formatMeeting(meeting);
    return m;
}

function formatMeeting(meeting) {
  return new Meeting(
    meeting.title,
    meeting.meeting_title,
    meeting.meeting_options.group_name,
    meeting.start_date,
    meeting.duration,
    meeting.location,
    meeting.published,
    meeting.conference,
    meeting.meeting_options.group_id
  );
}


async function retrieveMeeting(aad_id, id){

    const userProfile = await firestoreDb.pullUserQmApiToken(aad_id);
    const body = {"meeting_id" : id};
    const meeting = api.qmAPI(userProfile.qm_api_token, 'POST', 'meeting/get', body).then(async function(_meeting) {
        return _meeting;
    })

    return await meeting;

} 

module.exports = {
    getFormattedMeeting
}