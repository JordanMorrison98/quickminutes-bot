const {CardFactory} = require('botbuilder');

async function generateCard(){

    const card = CardFactory.adaptiveCard({
        "type": "AdaptiveCard",
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.2",
        "body": [
            {
                "type": "TextBlock",
                "text": "Need some help?",
                "wrap": true,
                "weight": "Bolder",
                "size": "Large"
            },
            {
                "type": "TextBlock",
                "text": "Choose one of the following options, or visit our [support page](https://support.quickminutes.com/portal/en-gb/kb/articles/microsoft-teams-integration-22-4-2020-1) to learn more about the QuickMinutes meeting assistant for Microsoft Teams.",
                "wrap": true,
                "spacing": "Medium"
            },
            {
                "type": "ActionSet",
                "spacing": "ExtraLarge",
                "actions": [
                    {
                        "type": "Action.Submit",
                        "title": "Configure notifications",
                        "data" : {"action": 'notifications_tapback'}
                    },
                    {
                        "type": "Action.Submit",
                        "title": "Take a tour",
                        "data" : {"action": 'take_a_tour'}
                    },
                    {
                        "type": "Action.Submit",
                        "title": "Remove notifications",
                        "data" : {"action": 'remove_notifications'}
                    },
                    {
                        "type": "Action.Submit",
                        "title": "Signout",
                        "data" : {"action": 'signout'}
                    }
                ]
            }
        ]
    });

    return card;


}

module.exports = {
    generateCard
}