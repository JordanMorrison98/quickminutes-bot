//import data from the NotificationConfig Activity Handler
const {CardFactory} = require('botbuilder');

const configHandler = require('../activityHandlers/notificationConfigActivityHandler.js');


async function generateCard(aad_id, activeNotifications){


        var choiceSet = []

        
        
        activeNotifications.forEach(notification => {
                choiceSet.push({"title": notification.groupName,"value": (notification.groupId + ',' + notification.groupName)})  
            })


    
        var card = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "Large",
                    "weight": "Bolder",
                    "text": "Remove Notifications" 
                },
                {
                    "type": "TextBlock",
                    "text": "Select a group to de-activate meetings notifcations for this channel.",
                    "wrap": true
                },
                {
                    "type": "Input.ChoiceSet",
                    "id": "selectedGroup",
                    "choices": choiceSet,
                    "isMultiSelect": false,
                    "wrap" : true,
                    "style" : "expanded"

                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.Submit",
                            "title": "🔕 Remove Notification",
                            "style": "positive",
                            "data" : {"action": 'remove_one_meeting_notification'},
                        }
                    ],
                    "spacing": "Medium",
                    "horizontalAlignment": "Center",
                    
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });

       
    return card;
}

module.exports = {
    generateCard
}