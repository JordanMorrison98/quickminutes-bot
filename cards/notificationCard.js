
const {CardFactory} = require('botbuilder');

async function generateCard(){


        var notificationCard = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "Large",
                    "weight": "Bolder",
                    "text": meetingPayload.meeting_title,
                    "wrap": true
                },
                {
                    "type": "TextBlock",
                    "text": meetingPayload.notification_type,
                    "wrap": true,
                    "size": "Medium",
                    "isSubtle": true,
                    "height": "stretch",
                    "spacing": "Small"
                },
                {
                    "type": "FactSet",
                    "facts": [
                        {
                            "title": "Location:",
                            "value": meetingPayload.location
                        }
                    ],
                    "separator": true,
                    "spacing": "Large"
                },
                {
                    "type": "FactSet",
                    "facts": [
                        {
                            "title": "Date: ",
                            "value": meetingPayload.start_date
                        }
                    ]
                },
                {
                    "type": "FactSet",
                    "facts": [
                        {
                            "title": "Time:",
                            "value": meetingPayload.start_time
                        }
                    ]
                },
                {
                    "type": "FactSet",
                    "facts": [
                        {
                            "title": "Duration:",
                            "value": meetingPayload.duration
                        }
                    ]
                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.OpenUrl",
                            "title": "📞  MS Teams Call",
                            "url": meetingPayload.conference_link,
                            "style": "positive"
                        }
                    ],
                    "spacing": "Large",
                    "horizontalAlignment": "Center",
                    "isVisible" : conference_link_bool
                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.OpenUrl",
                            "title": "📁  Meeting Pack",
                            "url": (url + "/group/" + meetingPayload.group_id + "/meeting/" + meetingId),
                            "style": "positive"
                        }
                    ],
                    "horizontalAlignment": "Center",
                    "spacing": "Large",
                    "isVisible" : meeting_pack_link_bool
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });



        return notificationCard;
       
    }


