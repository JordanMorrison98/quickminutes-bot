const {CardFactory} = require('botbuilder');

//just import the formatted meeting object data here
//then render the meeting summary card to the user

const meetingSummaryActivityHandler = require('../activityHandlers/meetingSummaryActivityHandler');

async function generateCard(aad_id, meetingId){

    const m = await meetingSummaryActivityHandler.getFormattedMeeting(aad_id, meetingId);

    if(process.env.baseUrl == "https://qmassistant-prod.azurewebsites.net"){
        const prodUrl = "https://quickminutes.com/app#";
        var url = prodUrl

    }else{
        const stagingUrl = "https://staging.quickminutes.com/app#";
        var url = stagingUrl;
    }

    //declare conditional variables
    var agenda_cta_str = "Create";

    if(m.agendaPresent === "Yes"){
        agenda_cta_str = "View";
    }
 

    const card = CardFactory.adaptiveCard({
        "type": "AdaptiveCard",
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.2",
        "body": [
            {
                "type": "TextBlock",
                "text": m.meetingTitle,
                "wrap": true,
                "size": "Large",
                "weight": "Bolder"
            },
            {
                "type": "TextBlock",
                "text": "Meeting Summary",
                "wrap": true,
                "size": "Medium",
                "spacing": "Small"
            },
            {
                "type": "FactSet",
                "facts": [
                    {
                        "title": "Location: ",
                        "value": m.location
                    }
                ],
                "separator": true,
                "spacing": "Medium"
            },
            {
                "type": "FactSet",
                "facts": [
                    {
                        "title": "Date:",
                        "value": m.meetingDate
                    }
                ]
            },
            {
                "type": "FactSet",
                "facts": [
                    {
                        "title": "Start Time: ",
                        "value": m.meetingTime
                    }
                ]
            },
            {
                "type": "FactSet",
                "facts": [
                    {
                        "title": "Duration: ",
                        "value": m.meetingDuration
                    }
                ]
            },
            {
                "type": "FactSet",
                "facts": [
                    {
                        "title": "Agenda:",
                        "value": m.agendaPresent
                    }
                ]
            },
            {
                "type": "ActionSet",
                "spacing": "Large",
                "actions": [
                    {
                        "type": "Action.OpenUrl",
                        "title": "📞  MS Teams Call",
                        "url": m.conferenceLink,
                        "style": "positive"
                    }
                ]
            },
            {
                "type": "ActionSet",
                "spacing": "Medium",
                "actions": [
                    {
                        "type": "Action.OpenUrl",
                        "title": "📝 " + agenda_cta_str + " Agenda",
                        "url": (url + "/group/" + m.groupId + "/meeting/" + meetingId),
                        "style": "positive"
                    }
                ]
            }
        ]
    });

    return card;


}

module.exports = {
    generateCard
}