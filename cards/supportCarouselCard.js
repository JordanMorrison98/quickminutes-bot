const {CardFactory} = require('botbuilder');

async function generateCard(){

        const supportCarouselCard = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "text": "Take a tour",
                    "wrap": true,
                    "weight": "Bolder",
                    "size": "Large"
                },
                {
                    "type": "Image",
                    "url": "https://i.ibb.co/8P2zJJD/illustration-placeholder.png\" alt=\"illustration-placeholder"
                },
                {
                    "type": "TextBlock",
                    "text": "Support",
                    "wrap": true,
                    "spacing": "Medium",
                    "size": "Medium",
                    "weight": "Bolder"
                },
                {
                    "type": "TextBlock",
                    "text": "Having trouble getting the most out of the QuickMinutes Assitant? Well don't worry, we've got you covered. Try any of these support buttons to assist you. ",
                    "wrap": true
                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.OpenUrl",
                            "title": "Demo Video",
                            "url": "https://www.loom.com/share/a96b2402dc6f40d4b8d011d8492ef8fe"
                        },
                        {
                            "type": "Action.OpenUrl",
                            "title": "Support Page",
                            "url": "https://support.quickminutes.com/portal/en-gb/kb/articles/microsoft-teams-integration-22-4-2020-1"
                        },
                        {
                            "type": "Action.Submit",
                            "title": "Feedback",
                            "data": {
                                "taskModule": "feedbackCard",
                                "msteams": {
                                "type": "task/fetch"
                                }
                            }     
                        }
                    ]
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });

        return supportCarouselCard;

    }

    module.exports = {
        generateCard
    }