const {CardFactory} = require('botbuilder');

async function generateCard(){

        const notificationCarouselCard = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "text": "Take a tour",
                    "wrap": true,
                    "weight": "Bolder",
                    "size": "Large"
                },
                {
                    "type": "Image",
                    "url" : "https://i.postimg.cc/kG67Q9dK/Quick-Minutes-Assistant-1.jpg"
                },
                {
                    "type": "TextBlock",
                    "text": "Meeting Notifications",
                    "wrap": true,
                    "spacing": "Medium",
                    "size": "Medium",
                    "weight": "Bolder"
                },
                {
                    "type": "TextBlock",
                    "text": "Need to notify your team about important meeting updates? I can help you with that! Simply type \"Notifications\" or tap the command button below.",
                    "wrap": true
                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.Submit",
                            "title": "Configure Notifications",
                            "data" : {"action": 'notifications_tapback'}
                        }
                    ]
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });

        return notificationCarouselCard;

    }

    module.exports = {
        generateCard
    }