const {CardFactory} = require('botbuilder');

async function generateCard(){

    const card = CardFactory.adaptiveCard({
        "type": "AdaptiveCard",
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.2",
        "body": [
            {
                "type": "TextBlock",
                "text": "Hey there, I'm the QM Assistant 👋",
                "wrap": true,
                "size": "Medium",
                "weight": "Bolder"
            },
            {
                "type": "TextBlock",
                "text": "Here are some of the things I can do:\n\n• Notify you when meetings are created for a group on QuickMinutes. \n\n• Notify you when meeting packs are published for a group -- includes call and meeting pack link. \n\n• Notify you when meetings for a group are updated or deleted. \n",
                "wrap": true,
                "spacing": "Medium"
            },
            {
                "type": "TextBlock",
                "text": "Ready to get started? Sign in!\n\nWant to learn more about this application? Take a quick tour. ",
                "wrap": true,
                "spacing": "Large"
            },
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "width": "stretch",
                        "items": [
                            {
                                "type": "ActionSet",
                                "actions": [
                                    {
                                        "type": "Action.Submit",
                                        "title": "Sign In",
                                        "data" : {"action": 'sign_in'},
                                    }
                                ],
                                "horizontalAlignment": "Center"
                            }


                            

                        ]
                    },
                    {
                        "type": "Column",
                        "width": "stretch",
                        "items": [
                            {
                                "type": "ActionSet",
                                "actions": [
                                    {
                                        "type": "Action.Submit",
                                        "title": "Take a tour",
                                        "data" : {"action": 'take_a_tour'}
                                    }
                                ],
                                "horizontalAlignment": "Center"
                            }
                        ]
                    }
                ],
                "spacing": "Large"
            }
        ]
    });


    return card;

}

module.exports = {
    generateCard
}