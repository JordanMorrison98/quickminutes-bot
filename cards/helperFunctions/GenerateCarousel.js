const { AttachmentLayoutTypes } = require('botbuilder');

const notificationCarouselCard = require('../notificationCarouselCard.js');
const supportCarouselCard = require('../supportCarouselCard.js');
 
 
 async function GenerateCarousel() {
    const notificationCarouselCardItem = await notificationCarouselCard.generateCard();
    const supportCarouselCardItem = await supportCarouselCard.generateCard();
    const carouselArr = [notificationCarouselCardItem, supportCarouselCardItem];

    const takeATourCard = {
        "attachments": carouselArr,
        "attachmentLayout": AttachmentLayoutTypes.Carousel
    };

    return takeATourCard;
}

module.exports = {
    GenerateCarousel
} 