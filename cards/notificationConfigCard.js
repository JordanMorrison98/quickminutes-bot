//import data from the NotificationConfig Activity Handler
const {CardFactory} = require('botbuilder');

const configHandler = require('../activityHandlers/notificationConfigActivityHandler.js');


async function generateCard(aad_id){


    var card = configHandler.retrieveGroups(aad_id).then(async function(data) {

        var choiceSet = []
        
        data.forEach(group => {
                choiceSet.push({"title": group.name,"value": (group.id + ',' + group.name)})  
            })

        let groups_present_in_profile_bool = true;
        let groups_not_present_in_profile_bool = false;

        if(data.length === 0){
            groups_present_in_profile_bool = false;
            groups_not_present_in_profile_bool = true;
        }

        let alertWarning = {
            "type": "Container",
            "spacing": "Medium",
            "style": "emphasis",
            "items": [
                {
                    "type": "TextBlock",
                    "text": "It looks like you don't have any groups in your QuickMinutes account. Follow this quick tutorial to get started.",
                    "wrap": true
                },
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.OpenUrl",
                            "title": "Watch video",
                            "url": "https://www.loom.com/share/8e33cd57e181433794c806789a20035c",
                        }
                    ]
                }
            ],
            "isVisible" : groups_not_present_in_profile_bool
        }

    
        var _card = CardFactory.adaptiveCard({
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "Large",
                    "weight": "Bolder",
                    "text": "Configure Notifications" 
                },
                {
                    "type": "TextBlock",
                    "text": "Select a group to activate meetings notifcations for this channel.",
                    "wrap": true
                },
                {
                    "type": "Input.ChoiceSet",
                    "id": "selectedGroup",
                    "choices": choiceSet,
                    "isMultiSelect": false,
                    "wrap" : true,
                    "style" : "expanded"

                },
                alertWarning,
                {
                    "type": "ActionSet",
                    "actions": [
                        {
                            "type": "Action.Submit",
                            "title": "🔔 Activate Notifications",
                            "style": "positive",
                            "data" : {"action": 'subscribe_to_meeting_notifications'},
                        }
                    ],
                    "spacing": "Medium",
                    "horizontalAlignment": "Center",
                    "isVisible" : groups_present_in_profile_bool
                    
                }
            ],
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2"
        });

        return _card;

        });

    return await card;
}

module.exports = {
    generateCard
}