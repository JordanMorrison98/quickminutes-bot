//get data passed from page generation
const { Meeting } = require('../../models/Meeting');

async function generateMeetingItem(meeting, groups){

    const _meeting = await populateMeetingWithGroupName(meeting, groups);

    const m = formatMeeting(_meeting);

    let meetingItem = {
        "type": "Container",
        "items": [
            {
                "type": "TextBlock",
                "text": m.meetingTitle,
                "wrap": true,
                "separator": true,
                "weight": "Bolder",
                "size": "Default",
            },
            {
                "type": "TextBlock",
                "text": m.group + " | " + m.meetingTime + " " + m.meetingDate + " | " + m.meetingDuration  +  " | Agenda: " + m.agendaPresent,
                "wrap": true,
                "spacing": "Small"
            }
        ],
        "separator": true,
        "spacing": "Medium",
        "selectAction": {
            "type": "Action.Submit",
            "data" : {"meetingId": meeting.id, "action" : "show_meeting_summary"}
        }
    }

    return meetingItem;

}

function formatMeeting(meeting) {
    return new Meeting(meeting.title, meeting.meeting_title, meeting.meeting_options.group_name, meeting.start_date, meeting.duration, meeting.location, meeting.published);
}

async function populateMeetingWithGroupName(meeting, groups){

    for(var i = 0; i < groups.length; i++){
        if(meeting.meeting_options.group_id === groups[i].id){
            meeting.meeting_options.group_name = groups[i].name;
        }
    }

    return meeting;
}


module.exports = {
    generateMeetingItem
}