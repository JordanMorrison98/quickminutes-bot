const itemGenerator = require('../meetingItemGenerator');
//get the first 5 meetings sortby date in DESC (nearest meeting date goes at the top of the list etc)
//use the meetingListActivityHandler to retrieve all the data you need >> api.js >> QM API

async function generate(meetings, groups){

    //make sure all meetings coming in here are populated with data. Filter out empty meeting objs i.e ' '

    const meetingItems = await retrieveAllMeetingItems(meetings, groups);

    var paginatedPages = [];

    var numberOfPages = Math.round(meetingItems.length/5);

    for(i=1; i<=numberOfPages; i++){
        var pageItems = await paginate(meetingItems, 5, i);
        var page = {
            type: "Container",
            id: i,
            items: pageItems,
          };

          paginatedPages.push(page);
    }
    
    //console.log(paginatedPages);

    return paginatedPages;

}

async function paginate(array, page_size, page_number) {
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
    return array.slice((page_number - 1) * page_size, page_number * page_size);
  }

async function retrieveAllMeetingItems(meetings, groups){

    var meetingItems = [];

    for (meeting of meetings){
        const meetingItem = await itemGenerator.generateMeetingItem(meeting, groups);
        meetingItems.push(meetingItem);
    }

    return meetingItems;
}



module.exports = {
    generate
}