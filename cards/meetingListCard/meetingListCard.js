const meetingsFactory = require('./pageGenerators/paginateItems');
const meetingListActivityHandler = require('../../activityHandlers/meetingListActivityHandler');

const {CardFactory} = require('botbuilder');

async function generateCard(aad_id){

    const meetingsObj = await meetingListActivityHandler.getUpcomingMeetings(aad_id);
    const paginatedMeetings = await meetingsFactory.generate(meetingsObj.meetings, meetingsObj.groups);
    const pages = await createPages(paginatedMeetings);
    
    var meetingListCard = CardFactory.adaptiveCard({
        "type": "AdaptiveCard",
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.2",
        "body": [
            {
                "type": "TextBlock",
                "text": "Meetings",
                "wrap": true,
                "size": "Large",
                "weight": "Bolder"
            },
            {
                "type": "TextBlock",
                "text": "Select a meeting for more details",
                "wrap": true,
                "spacing": "Default",
                "isSubtle": true
            },
            {
                "type": "TextBlock",
                "text": "Coming up soon...",
                "wrap": true,
                "weight": "Bolder",
                "spacing": "Medium",
                "color": "Accent"
            },
            pages[0],
            pages[1],
            pages[2],
            pages[3],
            pages[4],
            {
                "type": "ActionSet",
                "actions": [
                    {
                        "type": "Action.ToggleVisibility",
                        "title": "1",
                        "targetElements": [
                            {
                                "elementId": "1",
                                "isVisible": true
                            },
                            {
                                "elementId": "2",
                                "isVisible": false
                            },
                            {
                                "elementId": "3",
                                "isVisible": false
                            },
                            {
                                "elementId": "4",
                                "isVisible": false
                            },
                            {
                                "elementId": "5",
                                "isVisible": false
                            }
                        ]
                    },
                    {
                        "type": "Action.ToggleVisibility",
                        "title": "2",
                        "targetElements": [
                            {
                                "elementId": "1",
                                "isVisible": false
                            },
                            {
                                "elementId": "2",
                                "isVisible": true
                            },
                            {
                                "elementId": "3",
                                "isVisible": false
                            },
                            {
                                "elementId": "4",
                                "isVisible": false
                            },
                            {
                                "elementId": "5",
                                "isVisible": false
                            }
                        ]
                    },
                    {
                        "type": "Action.ToggleVisibility",
                        "title": "3",
                        "targetElements": [
                            {
                                "elementId": "1",
                                "isVisible": false
                            },
                            {
                                "elementId": "2",
                                "isVisible": false
                            },
                            {
                                "elementId": "3",
                                "isVisible": true
                            },
                            {
                                "elementId": "4",
                                "isVisible": false
                            },
                            {
                                "elementId": "5",
                                "isVisible": false
                            }
                        ]
                    },
                    {
                        "type": "Action.ToggleVisibility",
                        "title": "4",
                        "targetElements": [
                            {
                                "elementId": "1",
                                "isVisible": false
                            },
                            {
                                "elementId": "2",
                                "isVisible": false
                            },
                            {
                                "elementId": "3",
                                "isVisible": false
                            },
                            {
                                "elementId": "4",
                                "isVisible": true
                            },
                            {
                                "elementId": "5",
                                "isVisible": false
                            }
                        ]
                    },
                    {
                        "type": "Action.ToggleVisibility",
                        "title": "5",
                        "targetElements": [
                            {
                                "elementId": "1",
                                "isVisible": false
                            },
                            {
                                "elementId": "2",
                                "isVisible": false
                            },
                            {
                                "elementId": "3",
                                "isVisible": false
                            },
                            {
                                "elementId": "4",
                                "isVisible": false
                            },
                            {
                                "elementId": "5",
                                "isVisible": true
                            }
                        ]
                    }
                ],
                "spacing": "ExtraLarge"
            }
        ]
    });

    return meetingListCard;
}

async function createPages(paginatedMeetings){

    //create 5 default pages, then update the indexes that have data
    var pages = [];
    
    for (var i = 1; i <= 5; i++) {
      toString(i);
      var page = {
        "type":"Container",
        "id":i,
        "items":[
           {
              "type":"Container",
              "items":[
                 {
                    "type":"TextBlock",
                    "text":"No more upcoming meetings.",
                    "wrap":true
                 }
              ]
           }
        ]
     }
      pages.push(page);
    }

    //console.log(paginatedMeetings);

     //now replace the index of the default pages with the populated indexes of paginated meetings
    for(var x = 0; x < paginatedMeetings.length; x++){
        pages[x] = paginatedMeetings[x];
    }

    return pages;

}

module.exports = {
    generateCard
}