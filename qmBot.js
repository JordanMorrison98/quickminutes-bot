// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const {

    MessageFactory,
    TeamsActivityHandler,

} = require('botbuilder');

// Import dB
const firestoreDb = require('./services/firestoreDb.js');
// Import API
const api = require('./services/api.js');
//import analytics service
const { MixPanel } = require('./services/analytics/mixPanel');
//import services
const { Notifications } = require('./services/notifications.js');
//import factory classes and helper modules
const { AlertsFactory } = require('./utils/alertsFactory');
const FeaturePermissions = require('./utils/featurePermissions.js');
//import assets

//import activity classifications
const { ActivityClassifications } = require('./activityClassification/activityClassifications')
const intent = new ActivityClassifications();


//To-Do: Bulk import files
const configCardGenerator = require('./cards/notificationConfigCard.js');
const takeATour = require('./cards/helperFunctions/GenerateCarousel');
const meetingList = require('./cards/meetingListCard/meetingListCard');
const welcomecardGenerator = require('./cards/welcomeCard');
const helpCardGenerator = require('./cards/helpCard');
const removeNotificationsGenerator = require('./cards/removeNotificationsCard');
const meetingSummary = require('./cards/meetingSummaryCard');


// Welcomed User property name
const WELCOMED_USER = 'welcomedUserProperty';



/*
* Keep data access layer and presentation layer code out of here
* Only add code that's relevant to business logic e.g routing messages to the bot, LUIS, getting last user request etc
*/ 

class QmBot extends TeamsActivityHandler {
    constructor(userState) {
        super();

        // Creates a new user property accessor.
        // See https://aka.ms/about-bot-state-accessors to learn more about the bot state and state accessors.
        this.welcomedUserProperty = userState.createProperty(WELCOMED_USER);
        this.userState = userState;
        this.baseUrl = process.env.BaseUrl;
        
        // Registers an activity event handler for the message event, emitted for every incoming message activity.
        this.onMessage(async (context, next) => {

            //To-Do: Refactor this code so api token is already passed in the context object when authenticated
            try {
                const aad_id = context.activity.aad_id;
                const userProfile = await firestoreDb.pullUserQmApiToken(aad_id);
                this.apiToken = userProfile.qm_api_token != undefined ? userProfile.qm_api_token : null;
            } catch {
                this.apiToken = null;
            }

            try {
                //Check if an intent is present, if yes then map it to a feature
                //if not then show an error message to the user and log a failed request
                if(context.activity.text == 'meeting_notification_trigger'){
                    
                    var notificationCard = context.activity.data;
                    MixPanel.logEvent('Notification Sent', context, 'Success');   
                    await context.sendActivity({ attachments: [notificationCard] });

                } else if(context.activity.userIntent !== null){

                    //map extracted intent from user utterance to a feature
                    switch (context.activity.userIntent) {

                    case intent.UserIsSayingHello:
                        //displaying take a tour card for this intent for v1
                        var takeATourCard = await takeATour.GenerateCarousel();
                        MixPanel.logEvent('Take Tour', context, 'Success'); 
                        await context.sendActivity(takeATourCard);
                    break;

                    case intent.UserWantsHelp:
                        var helpCard = await helpCardGenerator.generateCard();
                        MixPanel.logEvent('Help Requested', context, 'Success'); 
                        await context.sendActivity({ attachments: [helpCard] });
                    break;

                    case intent.UserIsRequestingNotificationConfigurationCard:    
                        var configCard = await configCardGenerator.generateCard(context.activity.aad_id);
                        MixPanel.logEvent('Notification Configuration', context, 'Success');      
                        await context.sendActivity({ attachments: [configCard] });
                    break;

                    case intent.UserWantsToSubscribeToMeetingNotifications :
                           //send the context obj to the notifications service for meeting notification configuration
                           await this.setNotification(context);
                    break;

                    case intent.UserWantsToRemoveNotifications:
                        //if notifications are set, then render the remove notifications card
                        //if else, then render no notifications configured message
                        const activeNotifications = await Notifications.retrieveActiveNotifications(context);
                        if(activeNotifications.length !== 0){
                        MixPanel.logEvent('Notifications Deactivation', context, 'Success');      
                        var removeNotificationsCard = await removeNotificationsGenerator.generateCard(context.activity.aad_id, activeNotifications);
                        await context.sendActivity({ attachments: [removeNotificationsCard] });
                        }else{
                            var notConfigured = MessageFactory.text(`Looks like you don't have any notifications configured. Type **help** to configure notifications.`);
                        await context.sendActivity(notConfigured);    
                        }
                    
                        
                        
                       
                    break;

                    case intent.UserWantsRemoveOneNotification:
                        const result = await Notifications.removeNotifications(context);
                        if(result){
                            MixPanel.logEvent('Notification Removed', context, 'Success');      
                            var notificationRemovalMessage = await AlertsFactory.generateCard('Success' , `Meeting notifications successfully removed for **` + result.groupName + `**`);
                            await context.sendActivity({ attachments: [notificationRemovalMessage] });
                        }

                    break;

                    case intent.UserWantsToSignIn:
                        var signedInMessage = MessageFactory.text(`Looks like you're already signed in. Type **help** for more information.`);
                        await context.sendActivity(signedInMessage);    
                    break;
       
                    //QM Assistant v1.2.0 Features Start
                    case intent.UserWantsToViewMeetingList:
                        //if a LUIS can't extract and map a meeting entity from the request
                        //then show the user a paginated list of their upcoming meetings
                        var meetingListCard = await meetingList.generateCard(context.activity.aad_id);
                        MixPanel.logEvent('Meeting List', context, 'Success');  
                        //console.log(meetingListCard);
                        await context.sendActivity({ attachments: [meetingListCard] });
                    break;

                    case intent.UserWantsToViewMeetingSummary:
                        var meetingId = context.activity.value.meetingId;
                        var meetingSummaryCard = await meetingSummary.generateCard(context.activity.aad_id, meetingId);
                        MixPanel.logEvent('Meeting Summary', context, 'Success');           
                        await context.sendActivity({ attachments: [meetingSummaryCard] });
                    break;

                    
                    
                    //[placeholder for new feature in v2]
                    case 'AgendaSummary':
                        //var featureNotSupported = MessageFactory.text(`Sorry this feature isn't supported yet. Please try again later.`);
                        //await context.sendActivity(featureNotSupported);
                    break;

                    //[placeholder for new feature in v2]
                    case 'AgendaSummaryTapback':
                        //if a LUIS can't extract and map a meeting entity from the request
                        //then show the user a paginated list of their meetings
                    break;

                    //[placeholder for new feature in v2]
                    case 'MinutesSummary':
                    break;

                    //[placeholder for new feature in v2]
                    case 'MinutesSummaryTapback':
                        //if a LUIS can't extract and map a meeting entity from the request
                        //then show the user a paginated list of their meetings
                    break;

                    //VERSION 1.2.0 Features End
                            
                    
                    //https://stackoverflow.com/questions/59405348/microsoft-teams-bot-adaptive-card-carousel-deleting-a-card

                    case intent.UserIsRequestingTakeATourCard:
                        //generate cards and push into carousel array
                        var takeATourCard = await takeATour.GenerateCarousel();
                        MixPanel.logEvent('Take Tour', context, 'Success'); 
                        await context.sendActivity(takeATourCard);
                    break;
                        
                    //if the bot can't process the user intent (request), than send them this error message
                    default: 
                        console.error("Failed Request");
                        console.error(context);  
                        MixPanel.logEvent('Failed Request', context, 'Failed'); 
                        const replyActivity = MessageFactory.text(`Sorry I couldn't understand that request. Please type **help** for more information.`);
                        await context.sendActivity(replyActivity);
                    break;

                    }

                }else{
                    console.error("Failed Request");
                    MixPanel.logEvent('Failed Request', context, 'Failed'); 
                    const replyActivity = MessageFactory.text(`Sorry I couldn't understand that request. Please type **help** for more information.`);
                    await context.sendActivity(replyActivity);
                        
                }
                    
                //very important to call next to process the next activity
                await next();
    
            } catch (error) {
                console.error("Failed Request");
                MixPanel.logEvent('Failed Request', context, 'Failed'); 
                const replyActivity = MessageFactory.text(`Sorry I couldn't understand that request. Please type **help** for more information.`);
                await context.sendActivity(replyActivity);
            }

            
      
        });


         //https://github.com/microsoft/BotBuilder-Samples/blob/main/samples/javascript_nodejs/03.welcome-users/bots/welcomeBot.js

            // Sends welcome messages to conversation members when they join the conversation once.
            // Welcome message is only sent to conversation members who aren't the bot.
            this.onMembersAdded(async (context, next) => {

                const type = context.activity.conversation.conversationType;
                const gcWelcome = await this.welcomedUserProperty.get(context, false);
                const teamWelcome = await this.welcomedUserProperty.get(context, false);
                
                switch (type) {
                    case 'groupChat':
                        // Iterate over all new members added to the conversation
                        for (const idx in context.activity.membersAdded) {
                            if (context.activity.membersAdded[idx].id === context.activity.recipient.id && gcWelcome == false) {
                                // Set the flag indicating the bot has welcomed everyone in the groupchat
                                await this.welcomedUserProperty.set(context, true);
                                var welcomeCard = await welcomecardGenerator.generateCard();
                                await context.sendActivity({ attachments: [welcomeCard] });
                                
                            }
                        }
                        break

                    case 'personal':
                        var welcomeCard = await welcomecardGenerator.generateCard();
                        await context.sendActivity({ attachments: [welcomeCard] });  
                        break;

                    case 'channel':
                       // Iterate over all new members added to the conversation
                       for (const idx in context.activity.membersAdded) {
                        if (context.activity.membersAdded[idx].id === context.activity.recipient.id && teamWelcome == false) {
                            // Set the flag indicating the bot has welcomed everyone in the groupchat
                            await this.welcomedUserProperty.set(context, true);
                            var welcomeCard = await welcomecardGenerator.generateCard();
                            await context.sendActivity({ attachments: [welcomeCard] });                  
                        }
                    }         
                    default:
                        break;
                }

                // By calling next() you ensure that the next BotHandler is run.
                await next();
            });

       
        
        }


    //https://github.com/microsoft/BotBuilder-Samples/blob/main/samples/javascript_nodejs/54.teams-task-module/bots/teamsTaskModuleBot.js


    handleTeamsTaskModuleFetch(context, taskModuleRequest) {
                var aad_id = context.activity.from.aadObjectId
                var invokeType = context.activity.name;
                var invokeValue = taskModuleRequest.data.taskModule;
                if (invokeType === undefined) {
                    invokeType = null;
                }
                switch (invokeType) {

                    //not working TO DO
                    case "task/fetch": {
                        if (invokeValue !== undefined && invokeValue === "authCard") { 
                            // Return the specified task module response to the bot
                            var fetchTemplate = {
                                "task": {
                                    "type": "continue",
                                    "value": {
                                        "title": "Bot Activation",
                                        "height": 500,
                                        "width": 800,
                                        "url": "https://staging.quickminutes.com/microsoft_token_auth?redirectUri=" + process.env.baseUrl + "/callback?uid=" + aad_id + "&uid=" + aad_id,
                                        "fallbackUrl" : "https://staging.quickminutes.com/microsoft_token_auth?redirectUri=" + process.env.baseUrl + "/callback?uid=" + aad_id + "&uid=" + aad_id          
                                    }
                                }
                            };

                    
                            return fetchTemplate;
                     
                        } else if(invokeValue !== undefined && invokeValue === "feedbackCard"){

                            // Return the specified task module response to the bot
                            var fetchTemplate = {
                                "task": {
                                    "type": "continue",
                                    "value": {
                                        "title": "Feedback Form",
                                        "height": 500,
                                        "width": 800,
                                        "url": "https://forms.office.com/Pages/ResponsePage.aspx?id=apsTmgxhAEK5xUEEjlbGq88JU5CfLD5KgcCGUSP6V3hUNFBINzVUUFFEQ01WNTFJMFlSSlRETDMxNi4u",
                                        "fallbackUrl" : "https://forms.office.com/Pages/ResponsePage.aspx?id=apsTmgxhAEK5xUEEjlbGq88JU5CfLD5KgcCGUSP6V3hUNFBINzVUUFFEQ01WNTFJMFlSSlRETDMxNi4u"          
                                    }
                                }
                            };

                    
                            return fetchTemplate;

                        }

                        if (invokeValue !== undefined && invokeValue.data.taskModule === "adaptivecard") { 
                          
                        }

                        break;
                    }
                    case "task/submit": {
                        if (invokeValue.data !== undefined) {
                            // It's a valid task module response
                            var submitResponse = {
                                "task": {
                                    "type": "message",
                                    "value": "Task complete!",
                                }
                            };
                        // cb(null, fetchTemplates.submitMessageResponse, 200)
                        };
                    }
                }
    }


    async setNotification(context) {

        //TO-DO: Do some error handling logic here 

        if(typeof context.activity.value.selectedGroup !== "undefined"){
            var groupInfo = context.activity.value.selectedGroup.split(",");
            var groupId = groupInfo[0];
            var groupName = groupInfo[1];

            //map activity object to destination info
            
            var destinationInfo = this.parse(context)

            //check if user is group admin, if not then they can't config notifications for that group
            //show alert messages

            //TURNED OFF PERMISSIONS FOR VALIDATION PHASE BY MICROSOFT

            // var IsEnabled = await FeaturePermissions.featureIsEnabledForUser('notification_config' , context.activity.aad_id, groupId);
            
            // if(IsEnabled == true){

                const result = await Notifications.NotificationIsConfigured(context, groupId)

                if(result){
                    console.log("Notifications are already set for this group in the current channel.")
                    var alertActivity = await AlertsFactory.generateCard('Info' , `Looks like you have already set notifications for this group.`);
                    //To-Do: Add a param to the logEvent method: Error Message
                    MixPanel.logEvent('Notification Activated', context, 'Failed');
                    await context.sendActivity({ attachments: [alertActivity] });
                }else{
                    //save notification data to the dB
                    Notifications.storeInDB(groupId, groupName, destinationInfo);
                    var alertActivity = await AlertsFactory.generateCard('Success' , `Meeting notifications successfully configured for **` + groupName + `**`);

                    //turn on webhook preference programatically through the QM API
                    var userProfile = await firestoreDb.pullUserQmApiToken(context.activity.aad_id);
                    api.updateWebhookDetails(userProfile.qm_api_token, groupId)


                    MixPanel.logEvent('Notification Activated', context, 'Success');
                    await context.sendActivity({ attachments: [alertActivity] });
                }
            // }else if(IsEnabled == false){
            //     var alertActivity = await AlertsFactory.generateCard('Info' , `You can't configure notifications for this group as you do not have the correct permissions. Reach out to your group admin to fix this.`);
            //     //To-Do: Add a param to the logEvent method: Error Message
            //     MixPanel.logEvent('Notification Activated', context, 'Failed');
            //     await context.sendActivity({ attachments: [alertActivity] });
            // } else {
            //     var alertActivity = await AlertsFactory.generateCard('Error' , `Sorry we couldn't configure notifications for this group. Please try again.`);
            //     MixPanel.logEvent('Notification Activated', context, 'Failed');
            //     await context.sendActivity({ attachments: [alertActivity] });
            // }

            

        } else {

            // const eInfo = new TextEncoder().encode(icons.emojis.information);
            //const alertActivity = await AlertsFactory.generateCard('Info' , `Please select a group from the list before activating notifications`);
            var alertActivity = await AlertsFactory.generateCard('Warning' , `Please select a group from the list before activating notifications`);
            //To-Do: Add a param to the logEvent method: Error Message
            MixPanel.logEvent('Notification Activated', context, 'Failed');
            await context.sendActivity({ attachments: [alertActivity] });
        }

       
    }


    parse(context) {
        return {
            "from": context.activity.from,
            "conversation": context.activity.conversation,
            "recipient": context.activity.recipient,
            "serviceUrl" : context.activity.serviceUrl,
            "channelData": { "tenantId": context.activity.channelData.tenant }
        };
    }


}

module.exports.QmBot = QmBot;