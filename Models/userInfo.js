class userInfo {

    constructor(
        id,
        subscription_id,
        zoho_customer_id,
        email,
        title,
        first_name,
        last_name,
        timezone,
        language,
        avatar,
        active,
        reset,
        login_count,
        login_attempts,
        last_login,
        date_format,
        trial_plan_used,
        deleted,
        created_at,
        updated_at,
        is_super_admin,
        is_org_admin)
        {

            this.id = id;
            this.subscription_id =  subscription_id;
            this.zoho_customer_id = zoho_customer_id;
            this.email = email;
            this.title = title;
            this.first_name = first_name;
            this.last_name = last_name;
            this.timezone = timezone;
            this.language = language;
            this.avatar = avatar;
            this.active = active;
            this.reset = reset;
            this.login_count = login_count;
            this.login_attempts = login_attempts;
            this.last_login - last_login;
            this.date_format = date_format;
            this.trial_plan_used = trial_plan_used;
            this.deleted = deleted
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.is_super_admin = is_super_admin;
            this.is_org_admin = is_org_admin;
    }










 }

 module.exports.userInfo = userInfo;


 /*

 TO-DO: Re-evaulate need for setting class instances of user objects
 + Add integration & preferences to class constructor

 */

//  {
//     "id": 8447,
//     "subscription_id": 436,
//     "zoho_customer_id": "87359000001056001",
//     "email": "jordanmorrison476@gmail.com",
//     "title": "",
//     "first_name": "Jordan",
//     "last_name": "Morrison",
//     "timezone": "Europe/London",
//     "language": "Eng",
//     "avatar": "https://lh3.googleusercontent.com/a-/AOh14GgymaPE0OCiYsf0xM7WoMC4NjRkKQtQAVTREiUD5w",
//     "active": 1,
//     "reset": 0,
//     "login_count": 35,
//     "login_attempts": 0,
//     "last_login": "2020-10-18 19:27:00",
//     "date_format": "YYYY-MM-DD",
//     "trial_plan_used": 0,
//     "deleted": 0,
//     "created_at": "2020-09-10T10:57:21.000000Z",
//     "updated_at": "2020-10-18T19:27:00.000000Z",
//     "is_super_admin": false,
//     "is_org_admin": true,
//     "integrations": {
//         "id": 224,
//         "user_id": 8447,
//         "google_calendar": 0,
//         "microsoft_calendar": 0,
//         "deleted": 0,
//         "created_at": "2020-09-10T10:58:59.000000Z",
//         "updated_at": "2020-09-10T10:58:59.000000Z"
//     },
//     "preferences": {
//         "id": 2870,
//         "user_id": 8447,
//         "minutes_attachment": 0,
//         "added_to_domain": 0,
//         "email_general_announcement": 1,
//         "group_admins_announcement": 1,
//         "org_admins_announcement": 1,
//         "email_feature_announcement": 1,
//         "email_group_announcement": 1,
//         "app_general_announcement": 1,
//         "app_feature_announcement": 1,
//         "app_group_announcement": 1,
//         "deleted": 0,
//         "created_at": "2020-09-10T10:57:21.000000Z",
//         "updated_at": "2020-09-10T10:57:21.000000Z"
//     }
// }