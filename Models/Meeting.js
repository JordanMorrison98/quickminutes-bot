class Meeting {
  constructor(
    meetingTitle,
    spokenDate,
    group,
    startDate,
    duration,
    location,
    agenda,
    conferenceLink,
    groupId
  ) {
    this.meetingTitle = meetingTitle;
    this.spokenDate = spokenDate;
    this.group = group;
    this.startDate = startDate;
    this.duration = duration;
    this.location = location;
    this.agenda = agenda;
    this.conferenceLink = conferenceLink;
    this.groupId = groupId;
  }

  // Getter
  get meetingDate() {
    return this.getDate();
  }

  // Method
  getDate() {
    let dateTime = this.startDate.split(" ");
    //date format = dd/mm/yyyy
    const date = formatDate(dateTime);
    return date;
  }

  // Getter
  get meetingTime() {
    return this.getTime();
  }

  // Method
  getTime() {
    let dateTime = this.startDate.split(" ");
    //time format = hh:mm
    const time = formatTime(dateTime);
    return time;
  }

  // Getter
  get meetingDuration() {
    return this.getDuration(this.duration);
  }

  // Method
  getDuration() {
    //duration format = hh hour(s) mm min(s)
    let _duration = this.duration;
    const duration = formatDuration(_duration);
    return duration;
  }

  // Getter
  get agendaPresent() {
    return this.isMeetingPublished();
  }

  // Method
  isMeetingPublished() {
    //if published === 1 , then agenda is present. 
    if(this.agenda === 1){
        var published = 'Yes'
    }else{
        var published = 'No'
    }
    return published;
  }

  
}

//format helper functions
function formatDate(_dateTime) {
    var _date = _dateTime[0].split("-");
    var date = _date[2] + "/" + _date[1] + "/" + _date[0];
    return date;
  }


 function formatTime(_dateTime) {
    var _time = _dateTime[1].split(":");
    var time = _time[0] + ":" + _time[1];
    return time;
  }

  function formatDuration(data) {
    var _duration = data.split(":");
    var duration = " ";

    //TO-DO: Finish hours and minutes logic
    if (_duration[0] !== "00" && _duration[1] !== "00") {
      //duration has hours and minutes
      //hours and minutes, hour and minutes, hours and minute, hour and minute
      if (_duration[0] !== "00" && _duration[1] !== "00") {
        if (_duration[0].charAt(0) == "0") {
          // minutes > 2
          duration = _duration[0].charAt(1) + " minutes";
        } else {
          duration = _duration[0] + " minutes";
        }
      } else if (
        _duration[0].charAt(1) < "2" &&
        _duration[1].charAt(1) >= "2"
      ) {
        // < 2 hours and >=2 minutes
        duration = _duration[0] + " hour " + _duration[1] + " minutes";
      } else if (
        _duration[0].charAt(1) >= "2" &&
        _duration[1].charAt(1) < "2"
      ) {
        // >=2 hours and < 2 minutes
        duration = _duration[0] + " hours " + _duration[1] + " minute";
      } else if (_duration[0].charAt(1) < "2" && _duration[1].charAt(1) < "2") {
        // < 2 hours and < 2 minutes
        duration = _duration[0] + " hour " + _duration[1] + " minute";
      }
    }

    if (_duration[0] == "00") {
      //duration only has minutes
      //minutes or minute
      if (_duration[1] !== "00" || _duration[1] !== "01") {
        if (_duration[0].charAt(0) == "0") {
          // minutes > 2
          duration = _duration[0].charAt(1) + " minutes";
        } else {
          duration = _duration[0] + " minutes";
        }
      } else {
        // minutes <= 2
        duration = _duration[0].charAt(1) + " hour";
      }
    }

    if (_duration[1] == "00") {
      //duration only has hours
      //hours or hour
      if (_duration[0] == "00" || _duration[0] == "01") {
        // minutes <= 2
        if (_duration[0].charAt(0) == "0") {
          // minutes > 2
          duration = _duration[0].charAt(1) + " hour";
        } else {
          duration = _duration[0] + " hour";
        }
      } else {
        if (_duration[0].charAt(0) == "0") {
          // minutes > 2
          duration = _duration[0].charAt(1) + " hours";
        } else {
          duration = _duration[0] + " hours";
        }
      }
    }

    return duration;
  }

module.exports.Meeting = Meeting;
