class Group {
    constructor(
        id,
        subscription_id,
        name,
        avatar,
        description,
        deleted,
        created_at,
        updated_at
    )
    {

        this.id = id;
        this.subscription_id = subscription_id;
        this.name = name;
        this.avatar = avatar;
        this.description = description;
        this.deleted = deleted;
        this.created_at = created_at;
        this.updated_at = updated_at;
 
       /* Add Methods here */

    }
 }

 //var group001 = new Group(01, 23, "John Doe's Test Group");


 
 export default {
     Group
 }