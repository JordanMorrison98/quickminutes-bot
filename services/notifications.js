

//Import Utils
const { NotificationsFactory }  = require('../utils/notificationsFactory');
const FeaturePermissions = require('../utils/featurePermissions');
//import factory classes and helper modules
const { AlertsFactory } = require('../utils/alertsFactory');
//Import Additional Services
const firestoreDb = require('./firestoreDb.js');
const db = firestoreDb.db;
const firestore = firestoreDb.admin.firestore;
// Import API
const api = require('./api.js');


class Notifications {

    //https://firebase.google.com/docs/firestore/manage-data/add-data
    static async storeInDB(groupId, groupName, destinationInfo){

        const notificationDocRef = db.collection('notifications').doc(groupId);

        try {
            // Add a new notification destination record to the same groupId.
            const unionRes = await notificationDocRef.update({
                destination: firestore.FieldValue.arrayUnion({
                    groupId: groupId,
                    groupName: groupName,
                    destinationInfo: destinationInfo
                }),
                conversationIds : firestore.FieldValue.arrayUnion(destinationInfo.conversation.id)
            });
        } catch {
            //if a notification hasn't been set for a groupId, then create the first record i.e destination[0]
           const res = await notificationDocRef.set({
                destination: [{
                    groupId: groupId,
                    groupName: groupName,
                    destinationInfo: destinationInfo
                }],
                conversationIds : [destinationInfo.conversation.id]
            });     
        }

        

    }

    //https://firebase.google.com/docs/firestore/query-data/get-data
    static async getNotificationRecordsByGroupId(groupId, meetingId, notificationType, aad_id){ 

        const ref = db.collection('notifications').doc(groupId);
        const doc = await ref.get();
        if (!doc.exists) {
            console.error("Notification document doesn\'t exist for this group: " + groupId);
            return;
        } else {
           // console.log(doc.data());
            doc.data().destination.forEach(doc => {
                //iterate over each record associated with the group Id and send a notification 'x' times. 
                NotificationsFactory.send(doc, meetingId, notificationType, aad_id)
            });
        }
    }

    //https://firebase.google.com/docs/firestore/manage-data/delete-data
    static async retrieveActiveNotifications(context){

        //find all the records that have the same channel/destination id
        //then delete those fields from the dB

        // Create a reference to the cities collection
        const notificationsCollection = db.collection('notifications');
        // Create a query against the collection
        const queryRef = notificationsCollection.where('conversationIds', 'array-contains',  context.activity.conversation.id);
        /**
         * To-Do (09/03/2021)
         *  now that you can find active notification docs by conversationId
         *  use this method to solve noticationDeactivation (Remove Notification Configuration Card)
         *  and to solve notification info / error messages
         */
        //

        let activeNotificationsDocs = [];
        let activeNotificationRecords = [];
        const res = await queryRef.get();
        res.forEach((doc) => {
            activeNotificationsDocs.push(doc.data());
        });

        //console.log(activeNotificationsDocs);

        for(var i=0; i < activeNotificationsDocs.length; i++){

            let notificationDoc = activeNotificationsDocs[i];

            for(var x=0; x< notificationDoc.destination.length; x++){
                if(notificationDoc.destination[x].destinationInfo.conversation.id === context.activity.conversation.id){
                    activeNotificationRecords.push(notificationDoc.destination[x]);
                }
            } 
        }

        //now send the activeNotifications array to the notificationRemoveCard generator for rendering
        console.log(activeNotificationRecords);
        return activeNotificationRecords;
        
    
    }

    static async removeNotifications(context){

        let groupInfo = context.activity.value.selectedGroup.split(",");
        let groupId = groupInfo[0];
        let groupName = groupInfo[1];

        //find all the records that have the same channel/destination id
        //then delete those fields from the dB

        //https://firebase.google.com/docs/firestore/query-data/get-data
        const notificationsCollection = db.collection('notifications').doc(groupId);
        const doc = await notificationsCollection.get();
        if (!doc.exists) {
            console.log('No such document!');
          } else {
            console.log('Document data:', doc.data());
            const notifications = doc.data();
            
            for(var i=0; i < notifications.destination.length; i++){

                let notificationRecord = notifications.destination[i];
    
                if(notificationRecord.destinationInfo.conversation.id === context.activity.conversation.id){
                    //remove the notification record from the notification doc
                    notifications.destination.splice(i, 1)
                    //remove the conversation id from the notification record index array
                    notifications.conversationIds.splice(i, 1)
                }
                
            }

            
            const res = await notificationsCollection.set(
                notifications,
              { merge: true }
            );

            //now update the notification doc using the groupId as PK
            //use the new notifications object to populate the doc
            if(res){
                const result = {
                    groupName : groupName,
                    value :  true
                }
        
                return result;
            }else{
                const result = {
                    groupName : null,
                    value :  false
                }
                console.log("Notification record could not be removed.")
                return result;
            }

          }
      
        
    }

       
    static async NotificationIsConfigured(context, groupId){


         //https://firebase.google.com/docs/firestore/query-data/get-data
        // Create a reference to the cities collection
        const notificationsCollection = db.collection('notifications').doc(groupId);
        const doc = await notificationsCollection.get();
        if (!doc.exists) {
            console.log('No such document!');
          } else {
            //console.log('Document data:', doc.data());
            const notifications = doc.data();
            
            for(var i=0; i < notifications.destination.length; i++){

                let notificationRecord = notifications.destination[i];
    
                if(notificationRecord.destinationInfo.conversation.id === context.activity.conversation.id){
                   //return true because a notification is already configed for this groupId + channelId
                   return true
                }
                
            }

          }

            
    }

    /**
     * Refactor the set notification code from qmBot.js into here
     * Problem: the activity can't be returned to the bot from tbis file
     */

    // static async setNotification(context) {

    //     if(typeof context.activity.value.selectedGroup !== "undefined"){
    //         var groupInfo = context.activity.value.selectedGroup.split(",");
    //         var groupId = groupInfo[0];
    //         var groupName = groupInfo[1];

    //         //map activity object to destination info
            
    //         var destinationInfo = this.parse(context);

    //         //check if user is group admin, if not then they can't config notifications for that group
    //         //show alert messages

    //         var IsEnabled = await FeaturePermissions.featureIsEnabledForUser('notification_config' , context.activity.aad_id, groupId);
            
    //         if(IsEnabled == true){

    //             //save notification data to the dB   
    //             storeInDB(groupId, groupName, destinationInfo);
    //             var alertActivity = await AlertsFactory.generateCard('Success' , `Meeting notifications successfully configured for **` + groupName + `**`);

    //             //turn on webhook preference programatically through the QM API
    //             var userProfile = await firestoreDb.pullUserQmApiToken(context.activity.aad_id);
    //             api.updateWebhookDetails(userProfile.qm_api_token, groupId)


    //             MixPanel.logEvent('Notification Activated', context, 'Success');
    //             await context.sendActivity({ attachments: [alertActivity] });

    //         }else if(IsEnabled == false){
    //             var alertActivity = await AlertsFactory.generateCard('Info' , `You can't configure notifications for this group as you do not have the correct permissions. Reach out to your group admin to fix this.`);
    //             //To-Do: Add a param to the logEvent method: Error Message
    //             MixPanel.logEvent('Notification Activated', context, 'Failed');
    //             await context.sendActivity({ attachments: [alertActivity] });
    //         } else {
    //             var alertActivity = await AlertsFactory.generateCard('Error' , `Sorry we couldn't configure notifications for this group. Please try again.`);
    //             MixPanel.logEvent('Notification Activated', context, 'Failed');
    //             await context.sendActivity({ attachments: [alertActivity] });
    //         }

            

    //     } else {

    //         const eInfo = new TextEncoder().encode(icons.emojis.information);
    //         //const alertActivity = await AlertsFactory.generateCard('Info' , `Please select a group from the list before activating notifications`);
    //         const alertActivity = MessageFactory.text(`Please select a group from the list before activating notifications ⚠`);
    //         console.log(alertActivity);
    //         MixPanel.logEvent('Notification Activated', context, 'Failed');
    //         await context.sendActivity({ attachments: [alertActivity] });
    //     }

       
    // }

    // static parse(context) {
    //     return {
    //         "from": context.activity.from,
    //         "conversation": context.activity.conversation,
    //         "recipient": context.activity.recipient,
    //         "channelData": { "tenantId": context.activity.channelData.tenant }
    //     };
    // }
    





}

module.exports.Notifications = Notifications;