var axios = require('axios').default;

async function qmAPI(token, method, endpoint, body){

  //body object should be constructed before passing to this method
  const url = process.env.baseUrl === "https://qmassistant-prod.azurewebsites.net" ? process.env.PRODUCTION_API : process.env.STAGING_API;
  
  switch (method) {
    case 'POST':
      if(body === null){
        try{
          const resp = await axios({
            method: method,
            url: url + endpoint,
            headers:  { 
              'Accept': 'application/json, text/plain, */*', 
              'Content-Type': 'application/json;charset=UTF-8', 
              'Authorization' : token
              }
        });
      
        //console.log(resp.data);
        return resp.data;
      
        } catch (err) {
          // Handle Error Here
          console.error(err);
        }
      }else{
        try{
          const resp = await axios({
            method: method,
            url: url + endpoint,
            headers:  { 
              'Accept': 'application/json, text/plain, */*', 
              'Content-Type': 'application/json;charset=UTF-8', 
              'Authorization' : token
              },
              data: body
        });
      
        //console.log(resp.data);
        return resp.data;
      
        } catch (err) {
          // Handle Error Here
          console.error(err);
        }
      }
      
    case 'GET':
      try{
      const resp = await axios({
        method: method,
        url: url + endpoint,
        headers:  { 
          'Accept': 'application/json, text/plain, */*', 
          'Content-Type': 'application/json;charset=UTF-8', 
          'Authorization' : token
          }
    });
  
    //console.log(resp.data);
    return resp.data;
  
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
    
  
    default:
      break;
  }

}

//refactor this method and put in a seperate webhook update activity handler or notification config handler?


async function updateWebhookDetails(token, id){

  if(process.env.baseUrl == "https://qmassistant-prod.azurewebsites.net"){
    const prodUrl = process.env.PRODUCTION_API;
    var url = prodUrl

  }else{
    const stagingUrl = process.env.STAGING_API;
    var url = stagingUrl;
  }
  
  //api change means I have to get the group preferences object from qm api
  //then append data + update their profile it with webhook details
  try{
    const resp_get = await axios({
      method: 'POST',
      url: url + 'group_preference/get_group_preference',
      headers:  { 
        'Accept': 'application/json, text/plain, */*', 
        'Content-Type': 'application/json;charset=UTF-8', 
        'Authorization' : token
        },
      data : {"group_id" : id}
  });

  //console.log(resp.data);

  const group_pref = resp_get.data;

  group_pref.webhook_status = 1;
  group_pref.webhook_type = "MS-TEAMS-BOT";

    try{
      const resp_update = await axios({
        method: 'POST',
        url: url + 'group_preference/update_group_preference',
        headers:  { 
          'Accept': 'application/json, text/plain, */*', 
          'Content-Type': 'application/json;charset=UTF-8', 
          'Authorization' : token
          },
          data: group_pref

    });

    console.log(resp_update.data);

    return resp_update.data;

    } catch (err) {
      // Handle Error Here
      console.error(err);
    }


  } catch (err) {
    // Handle Error Here
    console.error(err);
  }



}



 module.exports = {
   qmAPI,
   updateWebhookDetails
 }