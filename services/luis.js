//https://docs.microsoft.com/en-us/azure/cognitive-services/luis/luis-get-started-get-intent-from-rest?pivots=programming-language-javascript
var requestPromise = require('request-promise');
var queryString = require('querystring');
//import activity classifications
const { ActivityClassifications } = require('../activityClassification/activityClassifications')
const intent = new ActivityClassifications();


async function processUserRequest(context){

        const LUIS_appId = process.env.LUIS_appId;

        const LUIS_predictionKey = process.env.LUIS_predictionKey;

        const LUIS_endpoint = "https://westeurope.api.cognitive.microsoft.com/";

        try {

            // Pass the user utterance to the QM Assistant Luis Model for natural language processing
            const utterance = parseUserUtterance(context.activity.text);
            //////////

            // Create query string
            const queryParams = {
                "show-all-intents": true,
                "verbose":  true,
                "query": utterance,
                "subscription-key": LUIS_predictionKey
            }

            // Create the URI for the REST call.
            const URI = `${LUIS_endpoint}luis/prediction/v3.0/apps/${LUIS_appId}/slots/production/predict?${queryString.stringify(queryParams)}`

            // Send the REST call.
            const response = await requestPromise(URI);

            // Display the response from the REST call.
            console.log("\n" + response);

            const responseObj = JSON.parse(response);

            const userIntent = await rulesEnginev1(responseObj);

            console.log("User request intent ==> " + userIntent + "\n");

            return userIntent;      

        } catch {
            
            return null;

        }

        function parseUserUtterance(utterance){

            try {   

                //remove all non-user request text from the user utterance
                //example #1: mentioning the bot in channels or groups adds <at> .... </at> to the utterance
                const regex = /(?<=<\/at>).*/; //remove the <at> mention tag from user utterance
                const parse = regex.exec(utterance);
                const query = parse[0].trim();

                //this query is okay for LUIS processing now          
                return query;
                
            } catch {

                //user utterance doesn't contain any breaking characters
                //in order words, this utterance is okay for LUIS processing by default
                const query = utterance;
                return query;
                
            }

            

        }

        async function rulesEnginev1(responseObj){

            const topIntent = responseObj.prediction.topIntent;
            const intents = responseObj.prediction.intents;
            const predictionConfidence = intents[topIntent].score;

            if(predictionConfidence > 0.75){
                return topIntent
            }else{
                return null;
            }

        }

}

function mapActionToIntent(context){

    try {
         
        switch (context.activity.value.action) {
        
            case 'notifications_tapback':
                //this action is triggered whenever the activate notifications button is pressed
                context.activity.userIntent = intent.UserIsRequestingNotificationConfigurationCard;
            break;

            case 'subscribe_to_meeting_notifications' :
                context.activity.userIntent = intent.UserWantsToSubscribeToMeetingNotifications;
            break;

            case 'take_a_tour':
                context.activity.userIntent = intent.UserIsRequestingTakeATourCard;
            break;
    
            case 'signout':
                context.activity.userIntent = intent.UserWantsToSignOut;
            break;

            case 'sign_in':
                context.activity.userIntent = intent.UserWantsToSignIn;
            break;

            case 'show_meeting_summary':
                context.activity.userIntent = intent.UserWantsToViewMeetingSummary;
            break;

            case 'remove_notifications':
                context.activity.userIntent = intent.UserWantsToRemoveNotifications;
            break;

            case 'remove_one_meeting_notification' :
                context.activity.userIntent = intent.UserWantsRemoveOneNotification;
            break;

           
        
            default:
                return;
        }

        } catch {
            return;
        }


}


module.exports = {
 
    processUserRequest,
    mapActionToIntent

  }
