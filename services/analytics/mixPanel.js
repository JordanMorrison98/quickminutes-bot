const Mixpanel = require('mixpanel');

//TO-DO: Put client secrets into .env file
const mixpanel = Mixpanel.init('853fb05499d44314a4d5499b01a95b14', { "host": "api-eu.mixpanel.com" });

// Import dB
const firestoreDb = require('../firestoreDb.js');


class MixPanel {

    static async logEvent(_feature, context, _status){

        const split = _feature.split(" ");
        const feature = split[0] + split[1];
        const user = context.activity.userProfile;

        mixpanel.track(_feature, {
            distinct_id: context.activity.aad_id,
            feature: 'qm.' + feature, 
            search_text: context.activity.text,
            name: user.name,
            email: user.email,
            status: _status,
            tenantid: user.tenantId,
            userRole: user.userRole,
            UPN: user.userPrincipalName
        });

        return;
        
    }

}

module.exports.MixPanel = MixPanel;