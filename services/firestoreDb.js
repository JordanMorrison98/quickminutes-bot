const admin = require('firebase-admin');
const api = require('./api.js')
const serviceAccount = require('../serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

//import mixpanel
const Mixpanel = require('mixpanel');
const mixpanel = Mixpanel.init('853fb05499d44314a4d5499b01a95b14', { "host": "api-eu.mixpanel.com" });

async function pullUserQmApiToken(aad_id){

  const userProfileRef = db.collection('users').doc(aad_id);
    const doc = await userProfileRef.get();
    if (!doc.exists) {
        return;

    } else {
       return doc.data();
    }  

}

async function createUserProfile(bearerToken, aad_id){

  const qmUserData = await api.qmAPI(bearerToken, 'GET', 'user/get', null)
  //const aad_id = qmUserData.aad_id; //use the users AadObjectId as the QM Assistant Account id
  const userProfileRef = db.collection('users').doc(aad_id);
  const doc = await userProfileRef.get();

  if (!doc.exists) {
      console.log('No profile exists for this user. Creating a new profile...');
      const newProfile = db.collection('users').doc(aad_id);
      newProfile.set({
          aad_id : aad_id,
          qm_api_token : bearerToken,
          qm_id : qmUserData.id,
          timezone : qmUserData.timezone,
          language : qmUserData.language
        }).then(console.log("User Profile with aad_id of " + aad_id + " created successfully"));

        //track user on Mixpanel for product usage analytics
        const USER_SIGNUP_DATE = new Date().toISOString();

        mixpanel.people.set( aad_id, {
          "$email": qmUserData.email,    // only reserved properties need the $
          "Sign up date": USER_SIGNUP_DATE,    // Send dates in ISO timestamp format (e.g. "2020-01-02T21:07:03Z")
          "USER_ID": aad_id,    
          "Name" : qmUserData.first_name + " " + qmUserData.last_name
        });

      return;

  } else {
      //update the user profile with a valid access token
      userProfileRef.update({
        qm_api_token : bearerToken
      });  
     return;
  }  

}

async function userHasToken(aad_id){

  const userProfileRef = db.collection('users').doc(aad_id);
  const doc = await userProfileRef.get();

  if (!doc.exists) {
      console.log('Qm user doesn\'t exist on the database.');
      return {result : false, aad_id: null};
  } else {

    if(doc.data().qm_api_token){

      //**REFACTOR THIS CODE**
      //update profile with teams data
      // userProfileRef.update({
      //   teamsInfo : user
      // });  
      return {result : true, aad_id: aad_id};
    }else{
      console.log('User doesn\'t have a valid QuickMinutes access token associated with their account.');
      return {result : false, aad_id: null};
    }

  }   

}

async function updateUserProfile(aad_id, bearerToken){

  const qmUserData = await api.getUserData(bearerToken);
  const userProfileRef = db.collection('users').doc(aad_id);

  const doc = await userProfileRef.get();
  if (!doc.exists) {
      console.log('Qm user doesn\'t exist on the database');
      return;
  } else {
     userProfileRef.update({
          qm_api_token : bearerToken,
          name : qmUserData.first_name + " " + qmUserData.last_name,
          qm_id : qmUserData.id,
          email : qmUserData.email,
          timezone : qmUserData.timezone,
          language : qmUserData.language
      });   

     console.log("User Profile Updated")
     return;
  }   

}

async function getUserProfile(aad_id){

  const userProfileRef = db.collection('users').doc(aad_id);

  const doc = await userProfileRef.get();
  if (!doc.exists) {
      console.log('No profile exists for this user. Creating a new profile...');
      const newProfile = db.collection('users').doc(aad_id);
      newProfile.set({
          aad_id : aad_id,
        });     
      return;

  } else {
     //return the user profile doc
     return doc.data();
  }  

}

async function signUserOut(aad_id){

   //https://github.com/firebase/snippets-node/blob/1438e9f92d4cd74807f681fa2a705c2e71df32b0/firestore/main/index.js#L336-L347
  
   // Get the `FieldValue` object
    const FieldValue = admin.firestore.FieldValue;
    // Create a document reference
    const userProfileRef = db.collection('users').doc(aad_id);
    // Remove the qm_api_token field from the document
    const res = await userProfileRef.update({
        qm_api_token: FieldValue.delete()
    }).then(console.log(aad_id + " signed out successfully"));

    return;

}


module.exports = {
  db,
  admin,
  pullUserQmApiToken,
  createUserProfile,
  userHasToken,
  updateUserProfile,
  getUserProfile,
  signUserOut,
}
